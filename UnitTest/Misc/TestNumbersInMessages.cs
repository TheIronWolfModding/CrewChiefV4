﻿using System;

using CrewChiefV4;
using CrewChiefV4.Audio;
using CrewChiefV4.NumberProcessing;

using FluentAssertions;
using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace UnitTest.Misc
{
    [TestClass]
    public class TestNumbersInMessages
    {
        [TestMethod]
        public void TestPortugueseTwocmd()
        {
            UserSettings.GetUserSettings().setProperty("use_naudio", false); // otherwise AudioPlayer Debug.Asserts
            AudioPlayer ap = new AudioPlayer();
            AudioPlayer.soundPackLanguage = "pt-br";
            NumberReader numberReader = NumberReaderFactory.GetNumberReader();
            //var x = MessageFragment.Integer(2, MessageFragment.Genders("pt-br", NumberReader.ARTICLE_GENDER.FEMALE));
            var duas = numberReader.GetIntegerSounds(2, true, false, NumberReader.ARTICLE_GENDER.FEMALE);
            duas.Count.Should().Be(1);
            duas[0].Should().NotBeNull();
            duas[0].Should().Be("numbers/duas", "Because two in Portuguese is 'duas'");
        }
    }
}
