﻿using System;

using CrewChiefV4;

using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace UnitTest.Misc
{
    [TestClass]
    public class TestSpeechTrace
    {
        [TestMethod]
        public void Instantiate()
        {
            CrewChief.SpeechTrace = new SpeechTrace();
        }

        [TestMethod]
        public void Tracing()
        {
            string recognisedText = "How's my fuel";
            CrewChief.SpeechTrace = new SpeechTrace();
            CrewChief.SpeechTrace.Add(recognisedText);
        }

        string filename = "test.cct";
        [TestMethod]
        public void Dumping()
        {
            string ticksWhenRead = "0"; // the initial value
            string recognisedText = "How's my fuel";
            CrewChief.SpeechTrace = new SpeechTrace();
            CrewChief.SpeechTrace.Add(recognisedText);
            CrewChief.SpeechTrace.Dump(filename);
        }

        [TestMethod]
        public void Playback()
        {
            string ticksWhenRead = "0"; // the initial value
            string recognisedText = "How's my fuel";
            CrewChief.SpeechTrace = new SpeechTrace();
            CrewChief.SpeechTrace.Add(recognisedText);

            CrewChief.SpeechTrace.Start(filename);
            var resultText = CrewChief.SpeechTrace.Get(ticksWhenRead);
            Assert.AreEqual(recognisedText, resultText);
        }
    }
}
