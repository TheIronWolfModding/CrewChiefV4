﻿using System;
using System.IO;
using CrewChiefV4;
using FluentAssertions;
using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace UnitTest.Misc.Volume
{
    class processChecking
    {
        internal static bool vlcCheck()
        {
            string processName = "vlc";
            var windows = (Win32Stuff.FindWindowsWithText(processName));
            if (windows.Count != 0)
            {
                if (Win32Stuff.GetWindowText(windows[0]) != processName)
                {
                    return true;
                }
                else
                {
                    Console.WriteLine("VLC MUST BE PLAYING A FILE");
                }
            }
            else
            {
                {
                    Console.WriteLine("VLC MUST BE RUNNING");
                }
            }
            return false;
        }
        internal static bool runningCheck(string processName)
        {
            var windows = (Win32Stuff.FindWindowsWithText(processName));
            if (windows.Count == 0)
            {
                Console.WriteLine($"{processName} MUST BE RUNNING");
            }
            return windows.Count != 0;
        }
    }
    [TestClass]
    public class TDD_VolumeDucking
    {
        [TestInitialize]
        public void init()
        {
            Log.LogMask |= Log.LogType.Debug;
            if (!processChecking.vlcCheck())
            {
                return;
            }
            string processName = "vlc";
            int duckPercent = 20;
            Func<ControlVolumeOfProcess> cvp = () => new ControlVolumeOfProcess(processName);
            cvp.Should().NotThrow<ArgumentException>();
            cvp.Should().NotBeNull($"{processName} is not running (and playing a file)");
        }

        [TestMethod]
        public void TestDuckingVolumeOfVLC_mustBePlaying()
        {
            if (!processChecking.vlcCheck())
            {
                return;
            }
            string processName = "vlc";

            int duckPercent = 20;
            var cvp = new ControlVolumeOfProcess(processName);
            Console.WriteLine($"Volume before {cvp.GetVolume()}");
            cvp.SetVolume(1.0f); // Set it to max (1.0)
            Console.WriteLine($"Volume after setting to max (1.0) {cvp.GetVolume()}");
            cvp.GetVolume().Should().Be(1.0f);
            float newVolume = cvp.DuckVolume(-duckPercent);
            Console.WriteLine($"Volume after ducking {duckPercent}% {cvp.GetVolume()}");
            newVolume.Should().Be(0.8f);
            cvp.TheProcess.ProcessName.Should().Be(processName);
            Console.WriteLine($"Process: {processName}, {cvp.TheProcess.MainWindowTitle}");
        }

        [TestMethod]
        public void TestMaxMinVolumeOfVLC_mustBePlaying()
        {
            if (!processChecking.vlcCheck())
            {
                return;
            }
            string processName = "vlc";
            var cvp = new ControlVolumeOfProcess(processName);
            cvp.SetVolume(1.01f);
            Console.WriteLine($"Volume after setting to (invalid) 1.01 is {cvp.GetVolume()}");
            cvp.GetVolume().Should().Be(1.0f);

            cvp.SetVolume(-0.01f);
            Console.WriteLine($"Volume after setting to (invalid) -0.01 is {cvp.GetVolume()}");
            cvp.GetVolume().Should().Be(0.0f);
        }
    }
    [TestClass]
    public class TestVolumeControlOfGame
    {
        [TestInitialize]
        public void Initialize()
        {
            Log.LogMask |= Log.LogType.Debug;
        }
        [TestMethod]
        public void TestNoProcess()
        {
            string processName = "NoSuchProcess";
            Func<ControlVolumeOfProcess> cvp = () => new ControlVolumeOfProcess(processName);
        }
        [TestMethod]
        public void TestDuckingVolumeOfRF2()
        {
            GameOrVoipVolume cvg;
            string processName = "rFactor2";
            if (!processChecking.runningCheck(processName))
            {
                return;
            }
            CrewChief.gameDefinition.processName = processName;
            GameOrVoipVolume.AudioDuckingEnabled = true;
            cvg = new GameOrVoipVolume(false);

            File.Exists(DataFiles.game_volumes).Should().BeTrue($"{DataFiles.game_volumes} has not been created");

            Console.WriteLine($"Volume before {cvg.GameProcess.GetVolume()}");
            cvg.GameProcess.SetVolume(1.0f); // Set it to max (1.0)
            Console.WriteLine($"Volume after setting to max (1.0) {cvg.GameProcess.GetVolume()}");
            cvg.GameProcess.GetVolume().Should().Be(1.0f);
            GameOrVoipVolume.DuckGameAudio();
            cvg.GameProcess.GetVolume().Should().Be(0.2f);
            GameOrVoipVolume.UnDuckGameAudio();
            cvg.GameProcess.GetVolume().Should().Be(1.0f);
            cvg.GameProcess.TheProcess.ProcessName.Should().Be(processName);
            Console.WriteLine($"Process: {processName}, {cvg.GameProcess.TheProcess.MainWindowTitle}");
        }
        [TestMethod]
        public void TestDuckingVolumeOfAMS2_notRunning()
        {
            GameOrVoipVolume cvg;
            string processName = "AMS2AVX";
            CrewChief.gameDefinition.processName = processName;
            GameOrVoipVolume.AudioDuckingEnabled = true;
            cvg = new GameOrVoipVolume(false);
            cvg.GameProcess.SetVolume(1.0f); // Not enabled
            GameOrVoipVolume.DuckGameAudio();
            GameOrVoipVolume.UnDuckGameAudio();
            GameOrVoipVolume.AudioDuckingEnabled = false;
            cvg = new GameOrVoipVolume(false);
            GameOrVoipVolume.DuckGameAudio();
            GameOrVoipVolume.UnDuckGameAudio();
            GameOrVoipVolume.VolumeControlEnabled = true;
            cvg = new GameOrVoipVolume(false);
            cvg.GameProcess.SetVolume(1.0f); // Not running so no effect
        }
        [TestMethod]
        public void TestMaxMinVolumeOfDiscord_process()
        {
            string processName = "Discord";
            if (!processChecking.runningCheck(processName))
            {
                return;
            }
            var cvp = new ControlVolumeOfProcess(processName);
            cvp.SetVolume(1.01f);
            Console.WriteLine($"Volume after setting to (invalid) 1.01 is {cvp.GetVolume()}");
            cvp.GetVolume().Should().Be(1.0f);

            cvp.SetVolume(-0.01f);
            Console.WriteLine($"Volume after setting to (invalid) -0.01 is {cvp.GetVolume()}");
            cvp.GetVolume().Should().Be(0.0f);
        }
        [TestMethod]
        public void TestMaxMinVolumeOfDiscord()
        {
            GameOrVoipVolume cvg;
            string processName = "Discord";
            if (!processChecking.runningCheck(processName))
            {
                return;
            }
            GameOrVoipVolume.VolumeControlEnabled = true;
            cvg = new GameOrVoipVolume(true);

            File.Exists(DataFiles.game_volumes).Should().BeTrue($"{DataFiles.game_volumes} has not been created");

            Console.WriteLine($"Volume before {cvg.VoipProcess.GetVolume()}");
            cvg.VoipProcess.SetVolume(1.0f); // Set it to max (1.0)
            Console.WriteLine($"Volume after setting to max (1.0) {cvg.VoipProcess.GetVolume()}");
            cvg.VoipProcess.GetVolume().Should().Be(1.0f);
            GameOrVoipVolume.DecreaseVoipVolume();
            cvg.VoipProcess.GetVolume().Should().Be(0.9f);
            GameOrVoipVolume.IncreaseVoipVolume();
            cvg.VoipProcess.GetVolume().Should().Be(1.0f);
        }
    }
}
