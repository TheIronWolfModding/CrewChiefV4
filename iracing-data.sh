#!/bin/sh

# This script (only tested on Linux) can be used to obtain data from the iRacing /data SDK.
# It is used, in particular, to generate the iracing_formation.json file, which is used
# to workaround bad telemetry data for rolling formation and restarts
# https://forums.iracing.com/discussion/53163/formation-side-of-the-road-bug-in-crewchief-or-iracing/p1
#
# ./iracing-data.sh https://members-ng.iracing.com/data/track/get | jq 'map({track_dirpath,track_id,start_on_left,restart_on_left})' > CrewChiefV4/iracing_formation.json
#
# Some more examples:
#
# ./iracing-data.sh https://members-ng.iracing.com/data/track/get > iracing-track.json
# ./iracing-data.sh https://members-ng.iracing.com/data/car/get > iracing-car.json
# ./iracing-data.sh https://members-ng.iracing.com/data/track/assets > iracing-track-assets.json
#
# The docs are available at
#
# - https://forums.iracing.com/discussion/15068/general-availability-of-data-api/p1
# - https://members-login.iracing.com/?ref=https%3A%2F%2Fmembers-ng.iracing.com%2Fdata%2Fdoc&signout=true

AUTH="$HOME/.iracing.cookies"

if [ -f "$AUTH" ] ; then
    LINK="$(curl -s -b "$AUTH" -c "$AUTH" "$@" | jq -r .link)"
    if [ "$LINK" = "null" ] ; then
        rm -f "$AUTH"
        echo "Auth failed, try again to log in"
        exit 1
    fi
    curl -s "$LINK"
else
    echo "You need to login first, and then try again."
    echo -n "iRacing username (email): "
    read EMAIL
    echo -n "Password: "
    read -s PASSWORD

    EMAILLOWER=$(echo -n "$EMAIL" | tr [:upper:] [:lower:])
    ENCODEDPW=$(echo -n $PASSWORD$EMAILLOWER | openssl dgst -binary -sha256 | openssl base64)
    BODY="{\"email\": \"$EMAIL\", \"password\": \"$ENCODEDPW\"}"
    curl -s -c "$AUTH" -X POST -H 'Content-Type: application/json' --data "$BODY" https://members-ng.iracing.com/auth > /dev/null
    echo "OK, try again"
fi
