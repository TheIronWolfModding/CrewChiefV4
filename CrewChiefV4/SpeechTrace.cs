﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using CrewChiefV4.Audio;
using Newtonsoft.Json;

namespace CrewChiefV4
{
    /// <summary>
    /// When saving a trace also save any recognised user speech
    /// separately using the same timetick. Then inject the speech
    /// when playing back the trace.
    /// </summary>
    public class SpeechTrace
    {
        /// <summary>
        /// Dictionary of ticksWhenRead : recognisedText
        /// </summary>
        private Dictionary<string, string> _dict;
        private bool _reading;
        private bool _notFound;

        public SpeechTrace()
        {
            _dict = new Dictionary<string, string>();
            _reading = false;
            _notFound = false;
        }

        /// <summary>
        /// Add recognised speech command to the trace
        /// </summary>
        /// <param name="recognisedText"></param>
        public void Add(string recognisedText)
        {
            var ticksWhenRead = CrewChief.ticksWhenRead;
            if (!String.IsNullOrEmpty(ticksWhenRead))
            {
                CrewChief.SpeechTrace._dict[ticksWhenRead] = recognisedText;
            }
        }

        /// <summary>
        /// If there is a speech trace dump it to a file to be read back later
        /// </summary>
        /// <param name="filePath">In the form PATH\GAMENAME_TIMESTAMP.cct</param>
        public void Dump(string filePath)
        {
            if (CrewChief.SpeechTrace._dict.Count > 0)
            {
                filePath = Path.ChangeExtension(filePath, "JSON");
                try
                {
                    string json = JsonConvert.SerializeObject(CrewChief.SpeechTrace._dict, Formatting.Indented);
                    File.WriteAllText(filePath, json);
                }
                catch (Exception ex)
                {
                    Log.Exception(ex, $"Error writing Speech Trace file '{filePath}'");
                }
            }
        }

        /// <summary>
        /// Set up reading of the speech trace
        /// </summary>
        /// <param name="filePath">In the form PATH\GAMENAME_TIMESTAMP.cct</param>
        public void Start(string filePath)
        {
            if (!_reading && !String.IsNullOrEmpty(filePath))
            {
                if (_notFound)
                    return;
                filePath = Path.ChangeExtension(filePath, "JSON");
                try
                {
                    if (File.Exists(filePath))
                    {
                        // Read the JSON data from the file
                        string jsonData = File.ReadAllText(filePath);

                        // Deserialize the JSON data into a dictionary
                        CrewChief.SpeechTrace._dict = JsonConvert.DeserializeObject<Dictionary<string, string>>(jsonData);
                        SubtitleManager.enableSubtitles = true; //TBD doesn't really work very well
                        SoundCache.lazyLoadSubtitles = true;
                        _reading = true;
                    }
                    else
                    {
                        Log.Warning($"Speech Trace file '{filePath}' not found");
                        _notFound = true;
                    }
                }
                catch (Exception ex)
                {
                    Log.Exception(ex, $"Error reading Speech Trace file '{filePath}'");
                }
            }
        }
        /// <summary>
        /// Looks up ticksWhenRead in SpeechTrace.dict and if it's there returns recognisedText 
        /// </summary>
        /// <param name="ticksWhenRead"></param>
        /// <returns>null if tick does not have a recognised speech</returns>
        public string Get(string ticksWhenRead)
        {
            string recognisedText = null;
            if (_reading)
            {
                CrewChief.SpeechTrace._dict.TryGetValue(ticksWhenRead, out recognisedText);
            }
            return recognisedText;
        }
    }
}
