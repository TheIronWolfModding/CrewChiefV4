using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using CrewChiefV4.GameState;
using CrewChiefV4.Audio;
using CrewChiefV4.NumberProcessing;
using System.Diagnostics;
using Newtonsoft.Json;
using System.IO;

namespace CrewChiefV4.Events
{
    class Fuel : AbstractEvent
    {
        #region consts
        private const String folderOneLapEstimate = "fuel/one_lap_fuel";

        private const String folderTwoLapsEstimate = "fuel/two_laps_fuel";

        private const String folderThreeLapsEstimate = "fuel/three_laps_fuel";

        private const String folderFourLapsEstimate = "fuel/four_laps_fuel";

        private const String folderHalfDistanceGoodFuel = "fuel/half_distance_good_fuel";

        private const String folderHalfDistanceLowFuel = "fuel/half_distance_low_fuel";

        private const String folderHalfTankWarning = "fuel/half_tank_warning";

        private const String folderTenMinutesFuel = "fuel/ten_minutes_fuel";

        private const String folderTwoMinutesFuel = "fuel/two_minutes_fuel";

        private const String folderFiveMinutesFuel = "fuel/five_minutes_fuel";

        private const String folderMinutesRemaining = "fuel/minutes_remaining";

        private const String folderLapsRemaining = "fuel/laps_remaining";

        private const String folderWeEstimate = "fuel/we_estimate";

        public const String folderPlentyOfFuel = "fuel/plenty_of_fuel";

        private const String folderLitresRemaining = "fuel/litres_remaining";

        private const String folderGallonsRemaining = "fuel/gallons_remaining";

        private const String folderOneLitreRemaining = "fuel/one_litre_remaining";

        private const String folderOneGallonRemaining = "fuel/one_gallon_remaining";

        private const String folderHalfAGallonRemaining = "fuel/half_a_gallon_remaining";

        private const String folderAboutToRunOut = "fuel/about_to_run_out";

        private const String folderLitresPerLap = "fuel/litres_per_lap";

        private const String folderGallonsPerLap = "fuel/gallons_per_lap";

        public const String folderLitres = "fuel/litres";

        public const String folderLitre = "fuel/litre";

        public const String folderGallons = "fuel/gallons";

        public const String folderGallon = "fuel/gallon";

        public const String folderWillNeedToStopAgain = "fuel/will_need_to_stop_again";

        private const String folderWillNeedToAdd = "fuel/we_will_need_to_add";

        private const String folderLitresToGetToTheEnd = "fuel/litres_to_get_to_the_end";

        private const String folderGallonsToGetToTheEnd = "fuel/gallons_to_get_to_the_end";

        // no 1 litres equivalent
        private const String folderWillNeedToAddOneGallonToGetToTheEnd = "fuel/need_to_add_one_gallon_to_get_to_the_end";

        private const String folderFuelWillBeTight = "fuel/fuel_will_be_tight";

        private const String folderFuelShouldBeOK = "fuel/fuel_should_be_ok";

        private const String folderFor = "fuel/for";
        private const String folderWeEstimateWeWillNeed = "fuel/we_estimate_we_will_need";

        // Note theserefer to 'absolute' times - 20 minutes from-race-start, not 20 minutes from-current-time.
        private const String folderFuelWindowOpensOnLap = "fuel/pit_window_for_fuel_opens_on_lap";
        private const String folderFuelWindowOpensAfterTime = "fuel/pit_window_for_fuel_opens_after";
        private const String folderAndFuelWindowClosesOnLap = "fuel/and_will_close_on_lap";
        private const String folderAndFuelWindowClosesAfterTime = "fuel/and_closes_after";

        private const String folderWillNeedToPitForFuelByLap = "fuel/pit_window_for_fuel_closes_on_lap";
        private const String folderWillNeedToPitForFuelByTimeIntro = "fuel/we_will_need_to_pit_for_fuel";
        private const String folderWillNeedToPitForFuelByTimeOutro = "fuel/into_the_race";

        private const float NO_FUEL_DATA = float.MaxValue;
        private const float HALF_LAP_RESERVE_DEFAULT = 2f;  // Allow 2L for half a lap unless we have a better estimate

        // check fuel use every 60 seconds
        private const int fuelUseSampleTime = 60;

        private const float litresPerGallon = 3.78541f;
        #endregion consts
        #region variables
        private static DateTime lastFuelCall = DateTime.MinValue;

        private List<float> persistedAverageUsagePerLap = new List<float>();
        private List<float> historicAverageUsagePerLap = new List<float>();
        private List<float> usagePerLapOutgoing = new List<float>();
        private ConditionsMonitor.TrackWetness worstConditionsOnLap = ConditionsMonitor.TrackWetness.UNKNOWN;

        private List<float> historicAverageUsagePerMinute = new List<float>();

        private float averageUsagePerLap;

        private float averageUsagePerMinute;

        // fuel in tank 15 seconds after game start
        private float initialFuelLevel;

        private int halfDistance;

        private float halfTime;

        private Boolean playedHalfTankWarning;

        private Boolean initialised;

        private Boolean playedHalfTimeFuelEstimate;

        private Boolean played1LitreWarning;

        private Boolean played2LitreWarning;

        // base fuel use by lap estimates on the last 3 laps
        private int fuelUseByLapsWindowLengthToUse = 3;
        private int fuelUseByLapsWindowLengthVeryShort = 5;
        private int fuelUseByLapsWindowLengthShort = 4;
        private int fuelUseByLapsWindowLengthMedium = 3;
        private int fuelUseByLapsWindowLengthLong = 2;
        private int fuelUseByLapsWindowLengthVeryLong = 1;

        // base fuel use by time estimates on the last 6 samples (6 minutes)
        private int fuelUseByTimeWindowLength = 6;

        private List<float> fuelLevelWindowByLap = new List<float>();

        private List<float> fuelLevelWindowByTime = new List<float>();

        private float gameTimeAtLastFuelWindowUpdate;

        private Boolean playedPitForFuelNow;

        private Boolean playedPitForFuelNowLastWarning;

        private Boolean crossedIntoSector3;

        private Boolean playedTwoMinutesRemaining;

        private Boolean playedFiveMinutesRemaining;

        private Boolean playedTenMinutesRemaining;

        private Boolean fuelUseActive;

        private float currentFuel = -1;

        private float fuelCapacity = 0;

        private float gameTimeWhenFuelWasReset = 0;

        private Boolean enableFuelMessages = UserSettings.GetUserSettings().getBoolean("enable_fuel_messages");

        private Boolean delayResponses = UserSettings.GetUserSettings().getBoolean("enable_delayed_responses");

        private float addAdditionalFuelLaps = UserSettings.GetUserSettings().getFloat("add_additional_fuel");

        private Boolean baseCalculationsOnMaxConsumption = UserSettings.GetUserSettings().getBoolean("prefer_max_consumption_in_fuel_calculations");

        private Boolean reportFuelLapsLeftInTimedRaces = UserSettings.GetUserSettings().getBoolean("report_fuel_laps_left_in_timed_races");

        public readonly Boolean experimentalFuelCalculations = UserSettings.GetUserSettings().getBoolean("experimental_fuel");
        // we could have voice commands that allow the user to change the fuel scale on the fly,
        // e.g. if they expect to be doing fuel saving or flat out depending on in-race strategy.
        private readonly float experimentalFuelPercentile = UserSettings.GetUserSettings().getFloat("experimental_fuel_percentile");
        private readonly float experimentalReserveLapsShort = UserSettings.GetUserSettings().getFloat("experimental_fuel_reserve_laps_short");
        private readonly float experimentalReserveLapsMedium = UserSettings.GetUserSettings().getFloat("experimental_fuel_reserve_laps_medium");
        private readonly float experimentalReserveLapsLong = UserSettings.GetUserSettings().getFloat("experimental_fuel_reserve_laps_long");
        private readonly float experimentalExtraLapsOval = UserSettings.GetUserSettings().getFloat("experimental_fuel_extra_laps_oval");

        private Boolean hasBeenRefuelled = false;

        // checking if we need to read fuel messages involves a bit of arithmetic and stuff, so only do this every few seconds
        private DateTime nextFuelStatusCheck = DateTime.MinValue;

        private DateTime nextFuelPitWindowOpenCheck = DateTime.MinValue;

        private TimeSpan fuelStatusCheckInterval = TimeSpan.FromSeconds(5);

        // this is used to indicate that the session either has a fixed number of laps
        // or we are inferring a fixed number of laps for a fixed time.
        // All logic should use these instead of checking the gamestate.
        //
        // Note that we may have a positive sessionNumberOfLaps but fixed number may be false,
        // which for some simulators means a session with a lap and time limit but we
        // prefer to treat it as a timed session.
        //
        // lapsRemaining = totalLaps - leaderCompletedLaps
        // (i.e. laps to go, including this one, for the player). When the player
        // crosses into the pit lane the data on the game state may be off by
        // one (depending on where the leader is) until they cross the start / finish line,
        // which could be before or after the pit stall. This field corrects for this error,
        // but all Events that make use of this class must be registered AFTER so that
        // they see the impact of the workaround.
        private volatile Boolean sessionHasFixedNumberOfLaps = false;
        private volatile int sessionNumberOfLaps = -1;
        private volatile int lapsRemaining = -1;
        private volatile float reserveMultiplier = -1;
        private volatile bool suppressLapsRemainingUpdate = false;
        private volatile float fuelOnPitEntry = -1;
        private DateTime lastFuelUseRecorded;

        // count laps separately for fuel so we always count incomplete and invalid laps
        private int lapsCompletedSinceFuelReset = 0;


        private float secondsRemaining = -1;

        private bool playedPitWindowEstimate = false;

        private bool playedPitWindowOpen = false;

        private int extraLapsAfterTimedSessionComplete = 0;

        private volatile bool greenLap;
        private Boolean sessionHasHadFCY = false;

        // in prac and qual, assume it's a low fuel run unless we know otherwise
        private Boolean onLowFuelRun = false;
        private float lapsForLowFuelRun = 4f;

        private float maxConsumptionPerLap = 0;

        // this is derived using the laptime on the lap where we've consumed the most fuel
        private float maxConsumptionPerMinute = 0;
        #endregion variables
        public Fuel(AudioPlayer audioPlayer)
        {
            this.audioPlayer = audioPlayer;
        }

        public override void clearState()
        {
            initialFuelLevel = 0;
            averageUsagePerLap = 0;
            halfDistance = -1;
            playedHalfTankWarning = false;
            initialised = false;
            halfTime = -1;
            playedHalfTimeFuelEstimate = false;
            fuelLevelWindowByLap = new List<float>();
            fuelLevelWindowByTime = new List<float>();
            gameTimeAtLastFuelWindowUpdate = 0;
            averageUsagePerMinute = 0;
            playedPitForFuelNow = false;
            playedPitForFuelNowLastWarning = false;
            crossedIntoSector3 = false;
            playedFiveMinutesRemaining = false;
            playedTenMinutesRemaining = false;
            playedTwoMinutesRemaining = false;
            played1LitreWarning = false;
            played2LitreWarning = false;
            currentFuel = 0;
            fuelUseActive = false;
            gameTimeWhenFuelWasReset = 0;
            hasBeenRefuelled = false;
            nextFuelStatusCheck = DateTime.MinValue;
            nextFuelPitWindowOpenCheck = DateTime.MinValue;
            sessionHasFixedNumberOfLaps = false;
            sessionNumberOfLaps = -1;
            lapsCompletedSinceFuelReset = 0;
            suppressLapsRemainingUpdate = false;

            reserveMultiplier = 1.0f;
            lapsRemaining = -1;
            secondsRemaining = -1;
            extraLapsAfterTimedSessionComplete = 0;
            fuelCapacity = 0;
            playedPitWindowOpen = false;
            playedPitWindowEstimate = false;
            greenLap = true;
            sessionHasHadFCY = false;

            persistedAverageUsagePerLap.Clear();
            historicAverageUsagePerLap.Clear();
            usagePerLapOutgoing.Clear();
            worstConditionsOnLap = ConditionsMonitor.TrackWetness.UNKNOWN;
            historicAverageUsagePerMinute.Clear();

            onLowFuelRun = false;
            lapsForLowFuelRun = 4f;
            fuelOnPitEntry = -1;

            maxConsumptionPerLap = 0;
            maxConsumptionPerMinute = 0;
            lastFuelUseRecorded = DateTime.MinValue;

            LapsRemaining = 0;
            LapsUntilRefuel = 0;
    }

    // fuel not implemented for HotLap/LonePractice modes
    public override List<SessionType> applicableSessionTypes
        {
            get { return new List<SessionType> { SessionType.Practice, SessionType.Qualify, SessionType.PrivateQualify, SessionType.Race, SessionType.LonePractice }; }
        }

        public override List<SessionPhase> applicableSessionPhases
        {
            get { return new List<SessionPhase> { SessionPhase.Countdown, SessionPhase.Green, SessionPhase.FullCourseYellow, SessionPhase.Checkered, SessionPhase.Finished }; }
        }

        private List<PersistedFuel> getAllPersistedFuel()
        {
            if (!File.Exists(DataFiles.fuel_usage))
            {
                return new List<PersistedFuel>();
            }
            return JsonConvert.DeserializeObject<List<PersistedFuel>>(File.ReadAllText(DataFiles.fuel_usage));
        }

        // to avoid growing the file infinitely, we only keep the last 5 sessions for this combo
        private void savePersistedFuelUsage(GameStateData currentGameState)
        {
            if (usagePerLapOutgoing.Count == 0)
            {
                return;
            }

            var game = CrewChief.gameDefinition.gameEnum.ToString();
            var carName = currentGameState.carName;
            var trackName = currentGameState.SessionData.TrackDefinition.name;

            var thisCombo = new List<PersistedFuel>();
            var entries = new List<PersistedFuel>();
            foreach (var datum in getAllPersistedFuel())
            {
                if (datum.isForThisCombo(game, carName, trackName))
                {
                    thisCombo.Add(datum);
                }
                else
                {
                    entries.Add(datum);
                }
            }

            var entry = new PersistedFuel();
            entry.game = game;
            entry.carName = carName;
            entry.trackName = trackName;
            entry.fuel = usagePerLapOutgoing;

            thisCombo.Insert(0, entry);
            entries.AddRange(thisCombo.Take(5).Reverse());

            Log.Fuel($"Persisting fuel usage for {usagePerLapOutgoing.Count} green laps");
            string jsonRaw = JsonConvert.SerializeObject(entries, Formatting.Indented);
            File.WriteAllText(DataFiles.fuel_usage, jsonRaw);
            usagePerLapOutgoing.Clear();
        }

        // there are potentially a lot of data points in the file, there's nothing stopping the user from
        // manually curating it with external data. But even so, we don't want historic data to overwhelm
        // the actual race data, so we randomly select 5 entries out of all the viable candidates.
        private List<float> getPersistedFuelUsage(GameStateData currentGameState)
        {
            var game = CrewChief.gameDefinition.gameEnum.ToString();
            var carName = currentGameState.carName;
            var trackName = currentGameState.SessionData.TrackDefinition.name;

            var relevant = new List<float>();
            foreach (var datum in getAllPersistedFuel())
            {
                if (datum.isForThisCombo(game, carName, trackName)) {
                    foreach (float data_point in datum.fuel)
                    {
                        if (data_point > 0.25)
                        {
                            // ignore fuel data that is very low because it is likely junk
                            // and if it's not junk then we'll figure out the usage
                            // pretty quickly anyway.
                            relevant.Add(data_point);
                        }
                    }
                }
            }
            Log.Fuel($"Loaded {relevant.Count} fuel usage samples for this combination");
            var rng = new Random();
            return relevant.OrderBy(x => rng.Next()).Take(5).ToList();
        }

        public bool fuelReportsInGallon
        {
            get { return !GlobalBehaviourSettings.useMetric; }
        }

        /// <summary>
        /// Do the fuel consumption calculations (when appropriate)
        /// Warn about fuel levels
        /// </summary>
        override protected void triggerInternal(GameStateData previousGameState, GameStateData currentGameState)
        {
            if (!GlobalBehaviourSettings.enabledMessageTypes.Contains(MessageTypes.FUEL) || Game.PCARS3 /* no fuel useage in pCars3*/)
            {
                return;
            }

            bool isPractice = currentGameState.SessionData.SessionType == SessionType.LonePractice || currentGameState.SessionData.SessionType == SessionType.Practice;
            bool isQualify = currentGameState.SessionData.SessionType == SessionType.Qualify || currentGameState.SessionData.SessionType == SessionType.PrivateQualify;
            bool isRace = currentGameState.SessionData.SessionType == SessionType.Race;

            if (experimentalFuelCalculations)
            {
                bool isFinish = currentGameState.SessionData.SessionPhase == SessionPhase.Finished && previousGameState.SessionData.SessionPhase != SessionPhase.Finished;
                bool enteredPits = !previousGameState.PitData.InPitlane && currentGameState.PitData.InPitlane;
                int green_laps = usagePerLapOutgoing.Count;

                if (isRace && isFinish)
                {
                    Log.Fuel($"EXPERIMENTAL fuel at the finish = {litresToUnits(currentGameState.FuelData.FuelLeft, true)}");
                }
                if (green_laps >= fuelUseByLapsWindowLengthToUse && ((isRace && (isFinish || enteredPits)) || (isPractice && enteredPits)))
                {
                     savePersistedFuelUsage(currentGameState);
                }
                else if (isPractice && enteredPits)
                {
                    // don't mix practice stints
                    usagePerLapOutgoing.Clear();
                }
            }
            if (currentGameState.SessionData.SessionPhase == SessionPhase.Finished)
            {
                return;
            }

            fuelUseActive = currentGameState.FuelData.FuelUseActive;
            if (Game.LMU)  //tbd LMU hack
                fuelUseActive = true;
            extraLapsAfterTimedSessionComplete = currentGameState.SessionData.ExtraLapsAfterTimedSessionComplete;
            // if the fuel level has increased, don't trigger
            if (currentFuel > -1 && currentFuel < currentGameState.FuelData.FuelLeft)
            {
                currentFuel = currentGameState.FuelData.FuelLeft;
                return;
            }
            if (currentGameState.SessionData.SessionHasFixedTime)
            {
                secondsRemaining = currentGameState.SessionData.SessionTimeRemaining;
            }
            else if (!suppressLapsRemainingUpdate)
            {
                lapsRemaining = currentGameState.SessionData.SessionLapsRemaining;
            }

            // see the note on lapsRemaining
            if (experimentalFuelCalculations)
            {
                if (previousGameState != null && !previousGameState.PitData.InPitlane && currentGameState.PitData.InPitlane)
                {
                    fuelOnPitEntry = currentGameState.FuelData.FuelLeft;
                    suppressLapsRemainingUpdate = true;
                    if (currentGameState.SessionData.SessionHasFixedTime || currentGameState.SessionData.OverallPosition == 1
                        || (currentGameState.getOpponentAtOverallPosition(1) != null && currentGameState.PositionAndMotionData.DistanceRoundTrack < currentGameState.getOpponentAtOverallPosition(1).DistanceRoundTrack))
                    {
                        // for fixed lap races, if the leader has already crossed the line ahead of us then we don't need to change anything
                        lapsRemaining = Math.Max(1, lapsRemaining - 1);
                    }
                }
                else if (previousGameState != null && previousGameState.PitData.InPitlane && !currentGameState.PitData.InPitlane)
                {
                    float diff = currentGameState.FuelData.FuelLeft - fuelOnPitEntry;
                    if (fuelOnPitEntry > 0 && diff > 0)
                    {
                        fuelOnPitEntry = -1;
                        Log.Fuel($"Fuel added during pitstop {litresToUnits(diff, true)}");
                    }
                    suppressLapsRemainingUpdate = false;
                }
            }

            currentFuel = currentGameState.FuelData.FuelLeft;
            fuelCapacity = currentGameState.FuelData.FuelCapacity;

            if (greenLap
                && (!(currentGameState.SessionData.SessionPhase == SessionPhase.Green || currentGameState.SessionData.SessionPhase == SessionPhase.Checkered)
                   || currentGameState.PitData.InPitlane
                   || currentGameState.PitData.JumpedToPits
                   || currentGameState.PitData.IsInGarage
                   || ((isPractice || isQualify) && !currentGameState.SessionData.CurrentLapIsValid)))
            {
                // Log.Fuel("Marking lap as NOT GREEN.");
                greenLap = false;
            }

            // only track fuel data after the session has settled down
            if (fuelUseActive && !GameStateData.onManualFormationLap &&
                // delaying by 15 seconds has an adverse affect on initial readings...
                (experimentalFuelCalculations || currentGameState.SessionData.SessionRunningTime > 15) &&

                ((currentGameState.SessionData.SessionType == SessionType.Race &&
                    (currentGameState.SessionData.SessionPhase == SessionPhase.Green ||
                     currentGameState.SessionData.SessionPhase == SessionPhase.FullCourseYellow ||
                     currentGameState.SessionData.SessionPhase == SessionPhase.Checkered)) ||

                 ((currentGameState.SessionData.SessionType == SessionType.Qualify ||
                   currentGameState.SessionData.SessionType == SessionType.PrivateQualify ||
                   currentGameState.SessionData.SessionType == SessionType.Practice ||
                   currentGameState.SessionData.SessionType == SessionType.HotLap ||
                   currentGameState.SessionData.SessionType == SessionType.LonePractice) &&

                    (currentGameState.SessionData.SessionPhase == SessionPhase.Green ||
                     currentGameState.SessionData.SessionPhase == SessionPhase.FullCourseYellow ||
                     currentGameState.SessionData.SessionPhase == SessionPhase.Countdown) &&

                    // don't process fuel data in prac and qual until we're actually moving:
                    currentGameState.PositionAndMotionData.CarSpeed > 10)))
            {
                if (currentGameState.SessionData.SessionPhase == SessionPhase.FullCourseYellow && currentGameState.SessionData.SessionType == SessionType.Race)
                {
                    sessionHasHadFCY = true;
                }
                if (!initialised ||
                    // fuel has increased by at least 1 litre
                    (fuelLevelWindowByTime.Count() > 0 && fuelLevelWindowByTime[0] > 0 && currentGameState.FuelData.FuelLeft > fuelLevelWindowByTime[0] + 1) ||
                    (fuelLevelWindowByLap.Count() > 0 && fuelLevelWindowByLap[0] > 0 && currentGameState.FuelData.FuelLeft > fuelLevelWindowByLap[0] + 1) ||
                    // special case for race session starting in the pitlane. When we exit the pit we might have much less fuel than we did at the start of the session because we took some out
                    // In this case, it'll be a race session with 0 laps completed at pit exit. AFAIK we can only take fuel out if we start from the pitlane
                    ((currentGameState.SessionData.SessionType != SessionType.Race || currentGameState.SessionData.CompletedLaps < 1)
                        && previousGameState != null && previousGameState.PitData.InPitlane && !currentGameState.PitData.InPitlane))
                {
                    // first time in, fuel has increased, or pit exit so initialise our internal state. Note we don't blat the average use data -
                    // this will be replaced when we get our first data point but it's still valid until we do.
                    fuelLevelWindowByTime = new List<float>();
                    fuelLevelWindowByLap = new List<float>();
                    fuelLevelWindowByTime.Add(currentGameState.FuelData.FuelLeft);
                    fuelLevelWindowByLap.Add(currentGameState.FuelData.FuelLeft);
                    initialFuelLevel = currentGameState.FuelData.FuelLeft;
                    greenLap = true;
                    gameTimeWhenFuelWasReset = currentGameState.SessionData.SessionRunningTime;
                    gameTimeAtLastFuelWindowUpdate = currentGameState.SessionData.SessionRunningTime;
                    playedPitForFuelNow = false;
                    playedPitForFuelNowLastWarning = false;
                    crossedIntoSector3 = false;
                    playedFiveMinutesRemaining = false;
                    playedTenMinutesRemaining = false;
                    playedTwoMinutesRemaining = false;
                    played1LitreWarning = false;
                    played2LitreWarning = false;
                    lapsCompletedSinceFuelReset = 0;
                    playedHalfTankWarning = false;
                    if (experimentalFuelCalculations)
                    {
                        if (historicAverageUsagePerLap.Count == 0)
                        {
                            try
                            {
                                persistedAverageUsagePerLap = getPersistedFuelUsage(currentGameState);
                            }
                            catch (Exception e)
                            {
                                Log.Fuel($"There was an error when opening the persisted fuel data: {e}");
                            }
                        }
                    }
                    else
                    {
                        historicAverageUsagePerLap.Clear();
                    }
                    historicAverageUsagePerMinute.Clear();
                    // set the onLowFuelRun if we're in prac / qual - asssume we're on a low fuel run until we know otherwise
                    if (isPractice || isQualify)
                    {
                        onLowFuelRun = true;
                        lapsForLowFuelRun = 4f;
                        switch (currentGameState.SessionData.TrackDefinition.trackLengthClass)
                        {
                            case TrackData.TrackLengthClass.LONG:
                                lapsForLowFuelRun = 3f;
                                break;
                            case TrackData.TrackLengthClass.VERY_LONG:
                                lapsForLowFuelRun = 2f;
                                break;
                        }
                        if (averageUsagePerLap > 0 && initialFuelLevel / averageUsagePerLap > lapsForLowFuelRun)
                        {
                            onLowFuelRun = false;
                        }
                    }
                    else
                    {
                        onLowFuelRun = false;
                    }
                    // if this is the first time we've initialised the fuel stats (start of session), get the half way point of this session
                    if (!initialised)
                    {
                        if (currentGameState.SessionData.TrackDefinition != null)
                        {
                            switch (currentGameState.SessionData.TrackDefinition.trackLengthClass)
                            {
                                case TrackData.TrackLengthClass.VERY_SHORT:
                                    fuelUseByLapsWindowLengthToUse = fuelUseByLapsWindowLengthVeryShort;
                                    break;
                                case TrackData.TrackLengthClass.SHORT:
                                    fuelUseByLapsWindowLengthToUse = fuelUseByLapsWindowLengthShort;
                                    break;
                                case TrackData.TrackLengthClass.MEDIUM:
                                    fuelUseByLapsWindowLengthToUse = fuelUseByLapsWindowLengthMedium;
                                    break;
                                case TrackData.TrackLengthClass.LONG:
                                    fuelUseByLapsWindowLengthToUse = fuelUseByLapsWindowLengthLong;
                                    break;
                                case TrackData.TrackLengthClass.VERY_LONG:
                                    fuelUseByLapsWindowLengthToUse = fuelUseByLapsWindowLengthVeryLong;
                                    break;
                            }
                        }
                        sessionNumberOfLaps = currentGameState.SessionData.SessionNumberOfLaps;
                        if (sessionNumberOfLaps > 1
                            &&
                            // rF2 has "Finish Criteria: Laps and Time", if that's selected then ignore laps
                            //  and calculate fuel based on time (sub-optimal but at least it won't run out of fuel)
                            !(Game.RF2_LMU
                                  && currentGameState.SessionData.SessionHasFixedTime)
                            )
                        {
                            sessionHasFixedNumberOfLaps = true;
                            if (halfDistance == -1)
                            {
                                halfDistance = (int)Math.Ceiling(sessionNumberOfLaps / 2f);
                            }
                        }
                        else if (currentGameState.SessionData.SessionTotalRunTime > 0)
                        {
                            sessionHasFixedNumberOfLaps = false;
                            if (halfTime == -1)
                            {
                                halfTime = (int)Math.Ceiling(currentGameState.SessionData.SessionTotalRunTime / 2f);
                            }
                        }
                        if (experimentalFuelCalculations)
                        {
                            // disable these messages
                            halfTime = -1;
                            halfDistance = -1;
                        }
                    }
                    Log.Fuel("Fuel level initialised, initialFuelLevel = " + litresToUnits(initialFuelLevel, false) + ", halfDistance = " + halfDistance + " halfTime = " + halfTime.ToString("0.00"));

                    initialised = true;
                }

                // OK, it's initialised, do the rest:
                if (previousGameState != null && previousGameState.SessionData.SectorNumber == 2 && currentGameState.SessionData.SectorNumber == 3 && playedPitForFuelNow)
                {
                    crossedIntoSector3 = true;
                }

                if (experimentalFuelCalculations)
                {
                    var c = currentGameState.Conditions.CurrentConditions;
                    if (c != null && c.TrackWetness > worstConditionsOnLap)
                    {
                        worstConditionsOnLap = c.TrackWetness;
                    }
                }

                if (currentGameState.SessionData.IsNewLap && currentGameState.FuelData.FuelLeft > 0)
                {
                    lapsCompletedSinceFuelReset++;
                    // completed a lap, so store the fuel left at this point:
                    fuelLevelWindowByLap.Insert(0, currentGameState.FuelData.FuelLeft);
                    // if we've got fuelUseByLapsWindowLength + 1 samples (note we initialise the window data with initialFuelLevel so we always
                    // have one extra), get the average difference between each pair of values

                    bool lastLapWasGreen = greenLap;
                    greenLap = true;

                    if (experimentalFuelCalculations)
                    {
                        if (currentGameState.SessionData.SessionHasFixedTime)
                        {
                            estimateLapsForFixedTimeRace(currentGameState);
                        }
                        if (currentGameState.SessionData.CompletedLaps > 0 && fuelLevelWindowByLap.Count >= 2 && lastLapWasGreen)
                        {
                            float thisLapFuelUse = fuelLevelWindowByLap[1] - fuelLevelWindowByLap[0];
                            // it is possible that junk data can get here (e.g. from telemetry missing a beat)
                            // so it is important to do some basic validation. We are still prone to errors when
                            // recording the first few laps of data so it is conceivable that a player somewhere
                            // may one day have to clean up or delete their persisted fuel data. The worst error
                            // would be to miss a lap and then get about double what we should have had.
                            if (thisLapFuelUse <= 0 || lastFuelUseRecorded == DateTime.MinValue)
                            {
                                Log.Fuel("EXPERIMENTAL fuel was not initialised correctly");
                            }
                            else if (lastFuelUseRecorded >= currentGameState.Now.Subtract(TimeSpan.FromSeconds(estimateFutureAverageLapTime(currentGameState) * 1.3f))
                                && (averageUsagePerLap <= 0 || Math.Abs(thisLapFuelUse - averageUsagePerLap) / averageUsagePerLap < 0.3))
                            {
                                // Log.Fuel("EXPERIMENTAL this lap fuel = " + litresToUnits(thisLapFuelUse, true));
                                historicAverageUsagePerLap.Add(thisLapFuelUse);
                                if (worstConditionsOnLap < ConditionsMonitor.TrackWetness.ModeratelyWet)
                                {
                                    usagePerLapOutgoing.Add(thisLapFuelUse);
                                }
                            }
                            else
                            {
                                Log.Fuel($"EXPERIMENTING rejecting fuel usage of {litresToUnits(thisLapFuelUse, true)}");
                            }
                        }
                        worstConditionsOnLap = ConditionsMonitor.TrackWetness.UNKNOWN;
                        lastFuelUseRecorded = currentGameState.Now;
                        updateAverageFuelUsage(currentGameState);
                    }
                    else
                    // only do this if we have a full window of data + one extra start point
                    if (fuelLevelWindowByLap.Count > fuelUseByLapsWindowLengthToUse)
                    {
                        averageUsagePerLap = 0;
                        for (int i = 0; i < fuelUseByLapsWindowLengthToUse; i++)
                        {
                            float thisLapFuelUse = fuelLevelWindowByLap[i + 1] - fuelLevelWindowByLap[i];
                            // the first element in this array is the lap just completed, so check if this is our max consumption per lap.
                            // At this point we must have completed some laps so should have a vague idea as to whether this lap is representative
                            if (i == 0 && thisLapFuelUse > maxConsumptionPerLap && canUseLastLapForMaxPerLapFuelConsumption(currentGameState))
                            {
                                maxConsumptionPerLap = thisLapFuelUse;
                                maxConsumptionPerMinute = 60 * thisLapFuelUse / currentGameState.SessionData.LapTimePrevious;
                            }
                            averageUsagePerLap += thisLapFuelUse;
                        }
                        averageUsagePerLap = averageUsagePerLap / fuelUseByLapsWindowLengthToUse;
                        historicAverageUsagePerLap.Add(averageUsagePerLap);
                        Log.Fuel("Fuel use per lap: windowed calc=" +
                            litresToUnits(averageUsagePerLap, true) +
                            ", max per lap=" +
                            litresToUnits(maxConsumptionPerLap, true) +
                            " left=" +
                            litresToUnits(currentGameState.FuelData.FuelLeft, false));
                    }
                    else
                    {
                        averageUsagePerLap = (initialFuelLevel - currentGameState.FuelData.FuelLeft) / lapsCompletedSinceFuelReset;
                        // this first calculation in the session is likely to be quite inaccurate so don't add it to the historic data
                        Log.Fuel("Fuel use per lap (basic calc) = " + litresToUnits(averageUsagePerLap, true) + " fuel left = " + litresToUnits(currentGameState.FuelData.FuelLeft, false));
                    }
                    // now check if we need to reset the 'on low fuel run' variable, do this on our 2nd flying lap
                    if (onLowFuelRun && lapsCompletedSinceFuelReset == 2 && averageUsagePerLap > 0 && initialFuelLevel / averageUsagePerLap > lapsForLowFuelRun)
                    {
                        onLowFuelRun = false;
                    }
                }

                if (experimentalFuelCalculations)
                {
                    // guard
                }
                else
                if (!currentGameState.PitData.InPitlane && currentGameState.FuelData.FuelLeft > 0 && currentGameState.SessionData.SessionRunningTime > gameTimeAtLastFuelWindowUpdate + fuelUseSampleTime)
                {
                    // it's x minutes since the last fuel window check
                    gameTimeAtLastFuelWindowUpdate = currentGameState.SessionData.SessionRunningTime;
                    fuelLevelWindowByTime.Insert(0, currentGameState.FuelData.FuelLeft);
                    // if we've got fuelUseByTimeWindowLength + 1 samples (note we initialise the window data with fuelAt15Seconds so we always
                    // have one extra), get the average difference between each pair of values

                    // only do this if we have a full window of data + one extra start point
                    if (fuelLevelWindowByTime.Count > fuelUseByTimeWindowLength)
                    {
                        averageUsagePerMinute = 0;
                        for (int i = 0; i < fuelUseByTimeWindowLength; i++)
                        {
                            averageUsagePerMinute += (fuelLevelWindowByTime[i + 1] - fuelLevelWindowByTime[i]);
                        }
                        averageUsagePerMinute = 60 * averageUsagePerMinute / (fuelUseByTimeWindowLength * fuelUseSampleTime);
                        historicAverageUsagePerMinute.Add(averageUsagePerMinute);
                        Log.Fuel("Fuel use per minute: windowed calc=" + litresToUnits(averageUsagePerMinute, true) +
                            ", max per min calc=" + litresToUnits(maxConsumptionPerMinute, true) +
                            " fuel left=" + litresToUnits(currentGameState.FuelData.FuelLeft, false));
                    }
                    else
                    {
                        averageUsagePerMinute = 60 * (initialFuelLevel - currentGameState.FuelData.FuelLeft) / (gameTimeAtLastFuelWindowUpdate - gameTimeWhenFuelWasReset);
                        // this first calculation in the session is likely to be quite inaccurate so don't add it to the historic data
                        Log.Fuel("Fuel use per minute (basic calc) = " + litresToUnits(averageUsagePerMinute, true) + " fuel left = " + litresToUnits(currentGameState.FuelData.FuelLeft, false));
                    }
                }

                // warnings for particular fuel levels
                if (enableFuelMessages && !onLowFuelRun)
                {
                    // warn when one/half a gallon left or two/one litre left
                    {	// (braces just to mark this clause)
						// Warn when two/one litre left but one/half a gallon left (approx 4L/2L)!
                        if (fuelReportsInGallon)
                        {
                            if (convertLitresToGallons(currentFuel) <= 1 && !played2LitreWarning)
                            {
                                // yes i know its not 2 litres but who really cares.
                                played2LitreWarning = true;
                                if (canPlayFuelMessage())
                                {
                                    audioPlayer.playMessage(new QueuedMessage("Fuel/level", 0, messageFragments: MessageContents(folderOneGallonRemaining), abstractEvent: this, priority: 10));
                                    lastFuelCall = currentGameState.Now;
                                }
                            }
                            else if (convertLitresToGallons(currentFuel) <= 0.5f && !played1LitreWarning)
                            {
                                //^^
                                played1LitreWarning = true;
                                if (canPlayFuelMessage())
                                {
                                    audioPlayer.playMessage(new QueuedMessage("Fuel/level", 0, messageFragments: MessageContents(folderHalfAGallonRemaining), abstractEvent: this, priority: 10));
                                    lastFuelCall = currentGameState.Now;
                                }
                            }
                        }
                        else
                        {
                            if (currentFuel <= 2 && !played2LitreWarning)
                            {
                                played2LitreWarning = true;
                                if (canPlayFuelMessage())
                                {
                                    audioPlayer.playMessage(new QueuedMessage("Fuel/level", 0, messageFragments: MessageContents(2, folderLitresRemaining), abstractEvent: this, priority: 10));
                                    lastFuelCall = currentGameState.Now;
                                }
                            }
                            else if (currentFuel <= 1 && !played1LitreWarning)
                            {
                                played1LitreWarning = true;
                                if (canPlayFuelMessage())
                                {
                                    audioPlayer.playMessage(new QueuedMessage("Fuel/level", 0, messageFragments: MessageContents(folderOneLitreRemaining), abstractEvent: this, priority: 10));
                                    lastFuelCall = currentGameState.Now;
                                }
                            }
                        }
                    }

                    // warnings for fixed lap sessions
                    float averageUsagePerLapToCheck = getConsumptionPerLap();
                    float averageUsagePerMinuteToCheck = getConsumptionPerMinute();
                    if (currentGameState.SessionData.IsNewLap && averageUsagePerLapToCheck > 0 &&
                        currentGameState.FuelData.FuelLeft > 0 &&
                        (sessionHasFixedNumberOfLaps || currentGameState.SessionData.SessionType == SessionType.HotLap || isPractice) &&
                        lapsCompletedSinceFuelReset > 0)
                    {
                        int estimatedFuelLapsLeft = (int)Math.Floor(currentGameState.FuelData.FuelLeft / averageUsagePerLapToCheck);
                        if (halfDistance != -1 && currentGameState.SessionData.SessionType == SessionType.Race && currentGameState.SessionData.CompletedLaps == halfDistance)
                        {
                            if (estimatedFuelLapsLeft <= halfDistance)
                            {
                                if (currentGameState.PitData.IsRefuellingAllowed)
                                {
                                    if (canPlayFuelMessage())
                                    {
                                        audioPlayer.playMessage(new QueuedMessage("Fuel/estimate", 0,
                                            messageFragments: MessageContents(RaceTime.folderHalfWayHome,
                                            folderWeEstimate,
                                            MessageFragment.Integer(estimatedFuelLapsLeft, MessageFragment.Genders("pt-br", NumberReader.ARTICLE_GENDER.FEMALE)),
                                            folderLapsRemaining),
                                            abstractEvent: this, priority: 7));
                                        lastFuelCall = currentGameState.Now;
                                    }
                                    else
                                    {
                                        audioPlayer.playMessage(new QueuedMessage("Fuel/halfWayHome", 0,
                                            messageFragments: MessageContents(RaceTime.folderHalfWayHome), abstractEvent: this, priority: 7));
                                    }
                                }
                                else
                                {
                                    audioPlayer.playMessage(new QueuedMessage(folderHalfDistanceLowFuel, 0, abstractEvent: this, priority: 7));
                                }
                            }
                            else
                            {
                                audioPlayer.playMessage(new QueuedMessage(folderHalfDistanceGoodFuel, 0, abstractEvent: this, priority: 5));
                            }
                        }
                        else if (lapsRemaining > 3 && estimatedFuelLapsLeft == 4)
                        {
                            Log.Fuel("4 laps fuel left, starting fuel = " + litresToUnits(initialFuelLevel, false) +
                                    ", current fuel = " + litresToUnits(currentGameState.FuelData.FuelLeft, false) + ", usage per lap = " + litresToUnits(averageUsagePerLapToCheck, true));
                            if (canPlayFuelMessage())
                            {
                                audioPlayer.playMessage(new QueuedMessage(folderFourLapsEstimate, 0, abstractEvent: this, priority: 3));
                                lastFuelCall = currentGameState.Now;
                            }
                        }
                        else if (lapsRemaining > 2 && estimatedFuelLapsLeft == 3)
                        {
                            Log.Fuel("3 laps fuel left, starting fuel = " + litresToUnits(initialFuelLevel, false) +
                                    ", current fuel = " + litresToUnits(currentGameState.FuelData.FuelLeft, false) + ", usage per lap = " + litresToUnits(averageUsagePerLapToCheck, true));
                            if (canPlayFuelMessage())
                            {
                                audioPlayer.playMessage(new QueuedMessage(folderThreeLapsEstimate, 0, abstractEvent: this, priority: 5));
                                lastFuelCall = currentGameState.Now;
                            }
                        }
                        else if (lapsRemaining > 1 && estimatedFuelLapsLeft == 2)
                        {
                            Log.Fuel("2 laps fuel left, starting fuel = " + litresToUnits(initialFuelLevel, false) +
                                    ", current fuel = " + litresToUnits(currentGameState.FuelData.FuelLeft, false) + ", usage per lap = " + litresToUnits(averageUsagePerLapToCheck, true));
                            if (canPlayFuelMessage())
                            {
                                audioPlayer.playMessage(new QueuedMessage(folderTwoLapsEstimate, 0, abstractEvent: this, priority: 7));
                                lastFuelCall = currentGameState.Now;
                            }
                        }
                        else if (lapsRemaining > 1 && estimatedFuelLapsLeft == 1)
                        {
                            // these lapsRemaining checks seem like they are off-by-one because they will say "X laps of fuel left"
                            // when there are X laps left in the race. But people might like to be reassured. We disable the last
                            // lap one only because it is very weird to get a call to pit the next lap when it's our last lap.
                            Log.Fuel("1 lap fuel left, starting fuel = " + litresToUnits(initialFuelLevel, false) +
                                    ", current fuel = " + litresToUnits(currentGameState.FuelData.FuelLeft, false) + ", usage per lap = " + litresToUnits(averageUsagePerLapToCheck, true));
                            audioPlayer.playMessage(new QueuedMessage(folderOneLapEstimate, 0, abstractEvent: this, priority: 10));
                            lastFuelCall = currentGameState.Now;
                            // if we've not played the pit-now message, play it with a bit of a delay - should probably wait for sector3 here
                            // but i'd have to move some stuff around and I'm an idle fucker
                            if (((crossedIntoSector3 && !playedPitForFuelNowLastWarning) || !playedPitForFuelNow) && lapsRemaining > 1)
                            {
                                playedPitForFuelNow = true;
                                if (crossedIntoSector3)
                                {
                                    playedPitForFuelNowLastWarning = true;
                                }
                                Log.Fuel("Pit this lap (fixed length race)");
                                audioPlayer.playMessage(new QueuedMessage(PitStops.folderMandatoryPitStopsPitThisLap, 0, secondsDelay: 10, abstractEvent: this, priority: 7));
                            }
                        }
                    }
                    else if (experimentalFuelCalculations)
                    {
                        // guard
                    }
                    // warnings for fixed time sessions - check every 5 seconds
                    else if (currentGameState.Now > nextFuelStatusCheck && currentGameState.FuelData.FuelLeft > 0 &&
                        currentGameState.SessionData.SessionNumberOfLaps <= 0 && currentGameState.SessionData.SessionTotalRunTime > 0 && averageUsagePerMinuteToCheck > 0)
                    {
                        float benchmarkLaptime = currentGameState.TimingData.getPlayerBestLapTime();
                        if (benchmarkLaptime <= 0)
                        {
                            benchmarkLaptime = currentGameState.TimingData.getPlayerClassBestLapTime();
                        }
                        nextFuelStatusCheck = currentGameState.Now.Add(fuelStatusCheckInterval);
                        if (halfTime != -1 && !playedHalfTimeFuelEstimate && currentGameState.SessionData.SessionTimeRemaining <= halfTime &&
                            currentGameState.SessionData.SessionTimeRemaining > halfTime - 30)
                        {
                            Log.Fuel("Half race distance. Fuel in tank = " + litresToUnits(currentGameState.FuelData.FuelLeft, false) +
                                ", average usage per minute = " + litresToUnits(averageUsagePerMinuteToCheck, true));
                            playedHalfTimeFuelEstimate = true;
                            if (currentGameState.SessionData.SessionType == SessionType.Race)
                            {
                                float slackAmount = averageUsagePerLapToCheck > 0 ? averageUsagePerLapToCheck : 2f;
                                // need a bit of slack in this estimate:
                                float fuelToEnd = averageUsagePerMinuteToCheck * (halfTime + benchmarkLaptime) / 60;
                                if (fuelToEnd > currentGameState.FuelData.FuelLeft)
                                {
                                    if (currentGameState.PitData.IsRefuellingAllowed)
                                    {
                                        if (canPlayFuelMessage())
                                        {
                                            int minutesLeft = (int)Math.Floor(currentGameState.FuelData.FuelLeft / averageUsagePerMinuteToCheck);
                                            audioPlayer.playMessage(new QueuedMessage("Fuel/estimate", 0,
                                                messageFragments: MessageContents(RaceTime.folderHalfWayHome, folderWeEstimate, minutesLeft, folderMinutesRemaining), abstractEvent: this, priority: 7));
                                            lastFuelCall = currentGameState.Now;
                                        }
                                        else
                                        {
                                            audioPlayer.playMessage(new QueuedMessage("Fuel/halfWayHome", 0,
                                                messageFragments: MessageContents(RaceTime.folderHalfWayHome), abstractEvent: this, priority: 7));
                                        }
                                    }
                                    else
                                    {
                                        audioPlayer.playMessage(new QueuedMessage(folderHalfDistanceLowFuel, 0, abstractEvent: this, priority: 7));
                                    }
                                }
                                else if (currentGameState.FuelData.FuelLeft - fuelToEnd <= slackAmount)
                                {
                                    audioPlayer.playMessage(new QueuedMessage("Fuel/estimate", 0,
                                        messageFragments: MessageContents(RaceTime.folderHalfWayHome, folderFuelWillBeTight), abstractEvent: this, priority: 7));
                                }
                                else
                                {
                                    audioPlayer.playMessage(new QueuedMessage(folderHalfDistanceGoodFuel, 0, abstractEvent: this, priority: 7));
                                }
                            }
                        }

                        float estimatedFuelMinutesLeft = currentGameState.FuelData.FuelLeft / averageUsagePerMinuteToCheck;
                        float estimatedFuelTimeRemaining = 2.0f;
                        estimatedFuelTimeRemaining = ((benchmarkLaptime / 60) * 1.1f) + ((benchmarkLaptime - currentGameState.SessionData.LapTimeCurrent) / 60);
                        if (estimatedFuelMinutesLeft < estimatedFuelTimeRemaining && (!playedPitForFuelNow || (!playedPitForFuelNowLastWarning && crossedIntoSector3)))
                        {
                            if (crossedIntoSector3)
                            {
                                playedPitForFuelNowLastWarning = true;
                            }
                            playedPitForFuelNow = true;
                            playedTwoMinutesRemaining = true;
                            playedFiveMinutesRemaining = true;
                            playedTenMinutesRemaining = true;
                            float cutoffForRefuelCall = 120;
                            //  needs to be <= as PlayerLapTimeSessionBest is initialized to -1
                            if (benchmarkLaptime != -1)
                            {
                                cutoffForRefuelCall = benchmarkLaptime * 2;
                            }
                            if (!currentGameState.PitData.InPitlane)
                            {
                                // call the player in to the pit for practice and race sessions, leave him out for qual (he may be doing a low fuel lap)
                                if ((currentGameState.SessionData.SessionType == SessionType.Race || isPractice)
                                    && currentGameState.SessionData.SessionTimeRemaining > cutoffForRefuelCall)
                                {
                                    Log.Fuel("Pit this lap (timed race)");
                                    audioPlayer.playMessage(new QueuedMessage("pit_for_fuel_now", 0,
                                        messageFragments: MessageContents(folderAboutToRunOut, PitStops.folderMandatoryPitStopsPitThisLap), abstractEvent: this, priority: 10));
                                }
                                else
                                {
                                    // going to run out, but don't call the player into the pits - it's up to him
                                    audioPlayer.playMessage(new QueuedMessage("about_to_run_out_of_fuel", 0, messageFragments: MessageContents(folderAboutToRunOut), abstractEvent: this, priority: 10));
                                }
                            }
                        }
                        if (estimatedFuelMinutesLeft <= 2 && estimatedFuelMinutesLeft > 1.8 && !playedTwoMinutesRemaining)
                        {
                            playedTwoMinutesRemaining = true;
                            playedFiveMinutesRemaining = true;
                            playedTenMinutesRemaining = true;
                            if (canPlayFuelMessage())
                            {
                                audioPlayer.playMessage(new QueuedMessage(folderTwoMinutesFuel, 0, abstractEvent: this, priority: 10));
                                lastFuelCall = currentGameState.Now;
                            }
                        }
                        else if (estimatedFuelMinutesLeft <= 5 && estimatedFuelMinutesLeft > 4.8 && !playedFiveMinutesRemaining)
                        {
                            playedFiveMinutesRemaining = true;
                            playedTenMinutesRemaining = true;
                            if (canPlayFuelMessage())
                            {
                                audioPlayer.playMessage(new QueuedMessage(folderFiveMinutesFuel, 0, abstractEvent: this, priority: 7));
                                lastFuelCall = currentGameState.Now;
                            }
                        }
                        else if (estimatedFuelMinutesLeft <= 10 && estimatedFuelMinutesLeft > 9.8 && !playedTenMinutesRemaining)
                        {
                            playedTenMinutesRemaining = true;
                            if (canPlayFuelMessage())
                            {
                                audioPlayer.playMessage(new QueuedMessage(folderTenMinutesFuel, 0, abstractEvent: this, priority: 3));
                                lastFuelCall = currentGameState.Now;
                            }
                        }
                        else if (!playedHalfTankWarning && currentGameState.FuelData.FuelLeft / initialFuelLevel <= 0.50 &&
                            currentGameState.FuelData.FuelLeft / initialFuelLevel >= 0.47 && !hasBeenRefuelled)
                        {
                            // warning message for fuel left - these play as soon as the fuel reaches 1/2 tank left
                            playedHalfTankWarning = true;
                            if (canPlayFuelMessage())
                            {
                                audioPlayer.playMessage(new QueuedMessage(folderHalfTankWarning, 0, abstractEvent: this, priority: 0));
                                lastFuelCall = currentGameState.Now;
                            }
                        }
                    }

                    // these are useful messages even in fixed timed races and races without pitstops because it prompts to
                    // consider car balance issues, such as brake bias.
                    //
                    // It would be good if we also had a "you have half a tank left" message looking at capacity rather than
                    // stint fuel, since that would be a more consistent message in terms of car balance.
                    if (experimentalFuelCalculations
                        && !playedHalfTankWarning
                        && !onLowFuelRun
                        && (currentGameState.SessionData.TrackDefinition.trackLengthClass > TrackData.TrackLengthClass.MEDIUM || lapsRemaining > 2)
                        && currentGameState.FuelData.FuelLeft / initialFuelLevel <= 0.50
                        && currentGameState.FuelData.FuelLeft / initialFuelLevel >= 0.47)
                    {
                        playedHalfTankWarning = true;
                        if (canPlayFuelMessage() && currentGameState.SessionData.CompletedLaps > 1)
                        {
                            audioPlayer.playMessage(new QueuedMessage(folderHalfTankWarning, 0, abstractEvent: this, priority: 0));
                            lastFuelCall = currentGameState.Now;
                        }
                    }

                    // detects pitstops enforced by fuel limit
                    if (!playedPitWindowEstimate && currentGameState.SessionData.SessionType == SessionType.Race &&
                        !currentGameState.PitData.HasMandatoryPitStop &&
                        previousGameState != null && previousGameState.SessionData.SectorNumber != currentGameState.SessionData.SectorNumber)
                    {
                        Tuple<int, int> predictedWindow = getPredictedPitWindow(currentGameState);
                        // item1 is the earliest minute / lap we can pit on, item2 is the latest. Note that item1 might be negative if
                        // we *could* have finished the race without refuelling (if we'd filled the tank). It might also be less than the
                        // number of minutes / laps completed

                        if (experimentalFuelCalculations)
                        {
                            int lapsUntilWindowCheck = 3;
                            if (currentGameState.SessionData.TrackDefinition.trackLengthClass < TrackData.TrackLengthClass.MEDIUM)
                            {
                                lapsUntilWindowCheck = 5;
                            }
                            else if (currentGameState.SessionData.TrackDefinition.trackLengthClass > TrackData.TrackLengthClass.MEDIUM)
                            {
                                lapsUntilWindowCheck = 1;
                            }

                            // Skip the check if we don't have enough data.
                            //
                            // Also, if we think we might make it on fuel without a stop, then cancel the pit window announcement.
                            // This avoids the situation when we pit with an extremely tight margin, and immediately get "pit window open".
                            if (currentGameState.SessionData.CompletedLaps < lapsUntilWindowCheck || getAdditionalFuelToEndOfRace(false, verbose: false) <= 0)
                            {
                                predictedWindow = new Tuple<int, int>(-1, -1);
                            }
                        }

                        if (predictedWindow.Item2 != -1)
                        {
                            if (sessionHasHadFCY && !experimentalFuelCalculations)
                            {
                                Console.WriteLine("skipping pit window announcement because there's been a full course yellow in this session so the data may be inaccurate");
                            }
                            else if (sessionHasFixedNumberOfLaps)
                            {
                                int lapLimit = 0;
                                if (!experimentalFuelCalculations)
                                {
                                    lapLimit = 2;
                                    switch (currentGameState.SessionData.TrackDefinition.trackLengthClass)
                                    {
                                        case TrackData.TrackLengthClass.VERY_LONG:
                                        case TrackData.TrackLengthClass.LONG:
                                            lapLimit = 1;
                                            break;
                                        case TrackData.TrackLengthClass.VERY_SHORT:
                                        case TrackData.TrackLengthClass.SHORT:
                                            lapLimit = 4;
                                            break;
                                    }
                                }
                                if (predictedWindow.Item2 > sessionNumberOfLaps - lapLimit)
                                {
                                    Console.WriteLine("Skipping fuel window announcement because we might make it on fuel");
                                }
                                else if (experimentalFuelCalculations && (playedPitForFuelNow || currentGameState.PitData.InPitlane))
                                {
                                    // this can happen at long tracks when laps remaining kicks in before the estimate
                                    // so we have technically already given the player this information (albeit in a different form)
                                    // or they already figured it out themselves and dived into the pits.
                                    Log.Fuel("Skipping fuel window announcement because we already know that we need to stop");
                                    playedPitWindowEstimate = true;
                                    playedPitForFuelNow = true;
                                    playedPitWindowOpen = true;
                                }
                                else if (experimentalFuelCalculations && predictedWindow.Item1 == currentGameState.SessionData.CompletedLaps)
                                {
                                    // corner case where we just calculated that the pit window opens this lap (can happen at longer tracks as early as lap 2).
                                    // we want to avoid playing an additional "pit window is open" message so we flag it as played.
                                    Log.Fuel("We know the pit window opens this lap, so bundle all the messages together");
                                    var fragments = MessageContents(PitStops.folderMandatoryPitStopsPitWindowOpen, folderWillNeedToPitForFuelByLap, predictedWindow.Item2);
                                    audioPlayer.playMessage(new QueuedMessage("Fuel/pit_window_for_fuel", 0, secondsDelay: Utilities.random.Next(8), messageFragments: fragments));
                                    playedPitWindowEstimate = true;
                                    playedPitWindowOpen = true;
                                    Strategy.playPitPositionEstimates = true;
                                }
                                // if item1 is < current minute but item2 is sensible, we want to say "pit window for fuel closes after X laps"
                                else if (predictedWindow.Item1 < currentGameState.SessionData.CompletedLaps)
                                {
                                    audioPlayer.playMessage(new QueuedMessage("Fuel/pit_window_for_fuel", 0, secondsDelay: Utilities.random.Next(8),
                                        messageFragments: MessageContents(folderWillNeedToPitForFuelByLap, predictedWindow.Item2)));
                                    playedPitWindowEstimate = true;
                                }
                                else
                                {
                                    audioPlayer.playMessage(new QueuedMessage("Fuel/pit_window_for_fuel", 0, secondsDelay: Utilities.random.Next(8),
                                        messageFragments: MessageContents(folderFuelWindowOpensOnLap, predictedWindow.Item1, folderAndFuelWindowClosesOnLap, predictedWindow.Item2)));
                                    playedPitWindowEstimate = true;
                                }
                            }
                            else
                            {
                                // sanity check
                                if (predictedWindow.Item2 > (currentGameState.SessionData.SessionTotalRunTime / 60) - 5)
                                {
                                    Console.WriteLine("Skipping fuel window announcement because we might make it on fuel");
                                }
                                // if item1 is < current minute, we want to say "pit window for fuel closes after X minutes"
                                else if (predictedWindow.Item1 < currentGameState.SessionData.SessionRunningTime / 60)
                                {
                                    audioPlayer.playMessage(new QueuedMessage("Fuel/pit_window_for_fuel", 0, secondsDelay: Utilities.random.Next(8),
                                        messageFragments: MessageContents(folderWillNeedToPitForFuelByTimeIntro, TimeSpanWrapper.FromMinutes(predictedWindow.Item2, Precision.MINUTES), folderWillNeedToPitForFuelByTimeOutro)));
                                    playedPitWindowEstimate = true;
                                }
                                else
                                {
                                    audioPlayer.playMessage(new QueuedMessage("Fuel/pit_window_for_fuel", 0, secondsDelay: Utilities.random.Next(8),
                                        messageFragments: MessageContents(folderFuelWindowOpensAfterTime, TimeSpanWrapper.FromMinutes(predictedWindow.Item1, Precision.MINUTES),
                                        folderAndFuelWindowClosesAfterTime, TimeSpanWrapper.FromMinutes(predictedWindow.Item2, Precision.MINUTES))));
                                    playedPitWindowEstimate = true;
                                }
                            }
                        }
                    }

                    if (playedPitWindowEstimate && !playedPitWindowOpen && currentGameState.SessionData.SessionType == SessionType.Race &&
                        !currentGameState.PitData.HasMandatoryPitStop &&
                        // check every 5 sec regardless if its a time limited or lap limited race, we want to know this as soon as possible. 
                        currentGameState.Now > nextFuelPitWindowOpenCheck)
                    {
                        nextFuelPitWindowOpenCheck = currentGameState.Now.Add(fuelStatusCheckInterval);
                        float litresNeeded = getAdditionalFuelToEndOfRace(true, verbose: false);
                        if (litresNeeded <= 0)
                        {
                            // we might have had to pit for repairs since we did the calculation and now the race is shorter for us
                            playedPitWindowOpen = true;
                        }
                        if (litresNeeded <= fuelCapacity - currentFuel)
                        {
                            Console.WriteLine($"Pit Window is now open, Litres Needed: {litresNeeded}");
                            playedPitWindowOpen = true;
                            audioPlayer.playMessage(new QueuedMessage(PitStops.folderMandatoryPitStopsPitWindowOpen, 0, abstractEvent: this, priority: 10));
                            Strategy.playPitPositionEstimates = true; // this might come before or instead of the "pit window is open" call, but that's ok.
                        }
                    }
                }
            }
        }

        /// <summary>
        /// Speak the mean fuel use per lap
        /// </summary>
        /// <param name="individualResponse">Speak just the fuel use...</param>
        /// <param name="messageFragments">...or add it to the stream of things being said</param>
        /// <returns>false: fuel use is less than 0.1 litres or gallons per lap</returns>
        private Boolean reportFuelConsumption(Boolean individualResponse, List<MessageFragment> messageFragments)
        {
            Boolean haveData = false;
            if (fuelUseActive && averageUsagePerLap > 0)
            {
                // round to 1dp
                float roundedAverageUsePerLap;
                if (fuelReportsInGallon)
                {
                    roundedAverageUsePerLap = ((float)Math.Round(convertLitresToGallons(averageUsagePerLap, false) * 10f)) / 10f;
                }
                else
                {
                    roundedAverageUsePerLap = ((float)Math.Round(averageUsagePerLap * 10f)) / 10f;
                }
                if (roundedAverageUsePerLap <= 0)
                {
                    // rounded fuel use is < 0.1 litres or gallons per lap - can't really do anything with this.
                    return false;
                }
                Tuple<int, int> wholeandfractional = Utilities.WholeAndFractionalPart(roundedAverageUsePerLap);
                QueuedMessage queuedMessage = null;
                haveData = true;

                if (wholeandfractional.Item2 > 0)
                {
                    if (fuelReportsInGallon)
                    {
                        queuedMessage = new QueuedMessage("Fuel/mean_use_per_lap", 0,
                                messageFragments: MessageContents(wholeandfractional.Item1, NumberReader.folderPoint, wholeandfractional.Item2, folderGallonsPerLap));

                        if (!individualResponse)
                        {
                            messageFragments.AddRange(MessageContents(wholeandfractional.Item1, NumberReader.folderPoint, wholeandfractional.Item2, folderGallonsPerLap));
                        }
                    }
                    else
                    {
                        queuedMessage = new QueuedMessage("Fuel/mean_use_per_lap", 0,
                                messageFragments: MessageContents(wholeandfractional.Item1, NumberReader.folderPoint, wholeandfractional.Item2, folderLitresPerLap));

                        if (!individualResponse)
                        {
                            messageFragments.AddRange(MessageContents(wholeandfractional.Item1, NumberReader.folderPoint, wholeandfractional.Item2, folderLitresPerLap));
                        }
                    }
                }
                else
                {
                    if (fuelReportsInGallon)
                    {
                        queuedMessage = new QueuedMessage("Fuel/mean_use_per_lap", 0,
                                messageFragments: MessageContents(wholeandfractional.Item1, folderGallonsPerLap));

                        if (!individualResponse)
                        {
                            messageFragments.AddRange(MessageContents(wholeandfractional.Item1, folderGallonsPerLap));
                        }
                    }
                    else
                    {
                        queuedMessage = new QueuedMessage("Fuel/mean_use_per_lap", 0,
                                messageFragments: MessageContents(wholeandfractional.Item1, folderLitresPerLap));

                        if (!individualResponse)
                        {
                            messageFragments.AddRange(MessageContents(wholeandfractional.Item1, folderLitresPerLap));
                        }
                    }                    
                }

                Debug.Assert(queuedMessage != null);
                if (individualResponse
                    && queuedMessage != null)
                {
                    if (this.delayResponses && Utilities.random.Next(10) >= 2 && SoundCache.availableSounds.Contains(AudioPlayer.folderStandBy))
                    {
                        this.audioPlayer.pauseQueueAndPlayDelayedImmediateMessage(queuedMessage, 5 /*lowerDelayBoundInclusive*/, 8 /*upperDelayBound*/);
                    }
                    else
                    {
                        this.audioPlayer.playMessageImmediately(queuedMessage);
                    }
                }
            }
            return haveData;
        }

        /// <summary>
        /// Used by "How much fuel for x laps?"
        /// </summary>
        /// <param name="numberOfLaps"></param>
        /// <returns></returns>
        private Boolean reportFuelConsumptionForLaps(int numberOfLaps)
        {
            Boolean haveData = false;
            if (fuelUseActive && averageUsagePerLap > 0)
            {
                // round up
                float totalUsage = 0f;
                if(fuelReportsInGallon)
                {
                    totalUsage = convertLitresToGallons(averageUsagePerLap * numberOfLaps, true);
                }
                else
                {
                    totalUsage = (float)Math.Ceiling(averageUsagePerLap * numberOfLaps);
                }
                if (totalUsage > 0)
                {
                    haveData = true;
                    // build up the message fragments the verbose way, so we can prevent the number reader from shortening hundreds to
                    // stuff like "one thirty two" - we always want "one hundred and thirty two"
                    List<MessageFragment> messageFragments = new List<MessageFragment>();
                    messageFragments.Add(MessageFragment.Text(folderFor));
                    messageFragments.Add(MessageFragment.Integer(numberOfLaps, false, MessageFragment.Genders("pt-br", NumberReader.ARTICLE_GENDER.FEMALE)));
                    messageFragments.Add(MessageFragment.Text(Battery.folderLaps));
                    messageFragments.Add(MessageFragment.Text(folderWeEstimateWeWillNeed));
                    if(fuelReportsInGallon)
                    {
                        // for gallons we want both whole and fractional part cause its a stupid unit.
                        Tuple<int, int> wholeandfractional = Utilities.WholeAndFractionalPart(totalUsage);
                        if (wholeandfractional.Item2 > 0)
                        {
                            messageFragments.AddRange(MessageContents(wholeandfractional.Item1, NumberReader.folderPoint, wholeandfractional.Item2, folderGallons));
                        }
                        else
                        {
                            int usage = Convert.ToInt32(wholeandfractional.Item1);
                            messageFragments.Add(MessageFragment.Integer(usage, false));
                            messageFragments.Add(MessageFragment.Text(usage == 1 ? folderGallon : folderGallons));
                        }
                    }
                    else
                    {
                        int usage = Convert.ToInt32(totalUsage);
                        messageFragments.Add(MessageFragment.Integer(usage, false));
                        messageFragments.Add(MessageFragment.Text(usage == 1 ? folderLitre : folderLitres));
                    }

                    if (messageFragments.Count > 0)
                    {
                        QueuedMessage fuelEstimateMessage = new QueuedMessage("Fuel/estimate", 0, messageFragments: messageFragments);

                        // play this immediately or play "stand by", and queue it to be played in a few seconds
                        if (delayResponses && Utilities.random.Next(10) >= 2 && SoundCache.availableSounds.Contains(AudioPlayer.folderStandBy))
                        {
                            audioPlayer.pauseQueueAndPlayDelayedImmediateMessage(fuelEstimateMessage, 5 /*lowerDelayBoundInclusive*/, 8 /*upperDelayBound*/);
                        }
                        else
                        {
                            audioPlayer.playMessageImmediately(fuelEstimateMessage);
                        }
                    }
                }
            }
            return haveData;
        }
        /// <summary>
        /// Used by "How much fuel for x hours/minutes?"
        /// </summary>
        /// <param name="hours"></param>
        /// <param name="minutes"></param>
        /// <returns></returns>
        private Boolean reportFuelConsumptionForTime(int hours, int minutes)
        {
            Boolean haveData = false;
            float averageUsagePerMinuteToCheck = getConsumptionPerMinute();
            if (fuelUseActive && averageUsagePerMinuteToCheck > 0)
            {
                int timeToUse = (hours * 60) + minutes;
                // round up
                float totalUsage = 0;
                if(fuelReportsInGallon)
                {
                    totalUsage = convertLitresToGallons(averageUsagePerMinuteToCheck * timeToUse, true);
                }
                else
                {
                    totalUsage = ((float)Math.Ceiling(averageUsagePerMinuteToCheck * timeToUse));
                }
                if (totalUsage > 0)
                {
                    haveData = true;
                    // build up the message fragments the verbose way, so we can prevent the number reader from shortening hundreds to
                    // stuff like "one thirty two" - we always want "one hundred and thirty two"
                    List<MessageFragment> messageFragments = new List<MessageFragment>();
                    messageFragments.Add(MessageFragment.Text(folderFor));
                    messageFragments.Add(MessageFragment.Time(TimeSpanWrapper.FromMinutes(timeToUse, Precision.MINUTES)));
                    messageFragments.Add(MessageFragment.Text(folderWeEstimateWeWillNeed));
                    if (fuelReportsInGallon)
                    {
                        // for gallons we want both whole and fractional part cause its a stupid unit.
                        Tuple<int, int> wholeandfractional = Utilities.WholeAndFractionalPart(totalUsage);
                        if (wholeandfractional.Item2 > 0)
                        {
                            messageFragments.AddRange(MessageContents(wholeandfractional.Item1, NumberReader.folderPoint, wholeandfractional.Item2, folderGallons));
                        }
                        else
                        {
                            int usage = Convert.ToInt32(wholeandfractional.Item1);
                            messageFragments.Add(MessageFragment.Integer(usage, false));
                            messageFragments.Add(MessageFragment.Text(usage == 1 ? folderGallon : folderGallons));
                        }
                    }
                    else
                    {
                        int usage = Convert.ToInt32(totalUsage);
                        messageFragments.Add(MessageFragment.Integer(usage, false));
                        messageFragments.Add(MessageFragment.Text(usage == 1 ? folderLitre : folderLitres));
                    }

                    if (messageFragments.Count > 0)
                    {
                        QueuedMessage fuelEstimateMessage = new QueuedMessage("Fuel/estimate", 0, messageFragments: messageFragments);
                        // play this immediately or play "stand by", and queue it to be played in a few seconds
                        if (delayResponses && Utilities.random.Next(10) >= 2 && SoundCache.availableSounds.Contains(AudioPlayer.folderStandBy))
                        {
                            audioPlayer.pauseQueueAndPlayDelayedImmediateMessage(fuelEstimateMessage, 5 /*lowerDelayBoundInclusive*/, 8 /*upperDelayBound*/);
                        }
                        else
                        {
                            audioPlayer.playMessageImmediately(fuelEstimateMessage);
                        }
                    }
                }
            }
            return haveData;
        }
        /// <summary>
        /// Give the best report of the fuel remaining in laps/time or just litres/gallons
        /// </summary>
        /// <param name="allowNoDataMessage">this is a fuel specific command response
        /// report "Plenty of fuel" if no consumption data is available </param>
        /// <param name="messageFragments">list that may be added to</param>
        /// <returns>Reported something about fuel</returns>
        private Boolean reportFuelRemaining(Boolean allowNoDataMessage, List<MessageFragment> messageFragments)
        {
            Boolean reportedSomething = false;
            float averageUsagePerLapToCheck = getConsumptionPerLap();

            void reportLapsOfFuelLeft()
            {
                int lapsOfFuelLeft = (int)Math.Floor(currentFuel / averageUsagePerLapToCheck);
                messageFragments.Add(MessageFragment.Text(folderWeEstimate));
                messageFragments.Add(MessageFragment.Integer(lapsOfFuelLeft, false, MessageFragment.Genders("pt-br", NumberReader.ARTICLE_GENDER.FEMALE)));
                messageFragments.Add(MessageFragment.Text(folderLapsRemaining));
            }

            if (initialised && currentFuel > -1)
            {
                if (sessionHasFixedNumberOfLaps && averageUsagePerLapToCheck > 0)
                {
                    reportedSomething = true;
                    int lapsOfFuelLeft = (int)Math.Floor(currentFuel / averageUsagePerLapToCheck);
                    if (lapsOfFuelLeft <= 1)
                    {
                        messageFragments.Add(MessageFragment.Text(folderAboutToRunOut));
                    }
                    else
                    {
                        reportLapsOfFuelLeft();
                    }
                }
                else if (averageUsagePerMinute > 0)
                {
                    reportedSomething = true;
                    int minutesOfFuelLeft = (int)Math.Floor(currentFuel / averageUsagePerMinute);
                    if (minutesOfFuelLeft <= 1)
                    {
                        messageFragments.Add(MessageFragment.Text(folderAboutToRunOut));
                    }
                    else if (reportFuelLapsLeftInTimedRaces && averageUsagePerLapToCheck > 0)
                    {
                        reportLapsOfFuelLeft();
                    }
                    else
                    {
                        messageFragments.Add(MessageFragment.Text(folderWeEstimate));
                        messageFragments.Add(MessageFragment.Integer(minutesOfFuelLeft, false));
                        messageFragments.Add(MessageFragment.Text(folderMinutesRemaining));
                    }
                }
                if (CrewChief.currentGameState != null && CrewChief.currentGameState.Now != null)
                    lastFuelCall = CrewChief.currentGameState.Now;
            }
            if (!reportedSomething)
            {
                if (!fuelUseActive && allowNoDataMessage)
                {
                    reportedSomething = true;
                    messageFragments.Add(MessageFragment.Text(folderPlentyOfFuel));
                } // else report amount of fuel
                else if (fuelReportsInGallon)
                {
                    if (convertLitresToGallons(currentFuel) >= 2)
                    {
                        reportedSomething = true;
                        // for gallons we want both whole and fractional part cause its a stupid unit.
                        Tuple<int, int> wholeandfractional = Utilities.WholeAndFractionalPart(convertLitresToGallons(currentFuel, true));
                        if (wholeandfractional.Item2 > 0)
                        {
                            messageFragments.AddRange(MessageContents(wholeandfractional.Item1, NumberReader.folderPoint, wholeandfractional.Item2, folderGallonsRemaining));
                        }
                        else
                        {
                            messageFragments.Add(MessageFragment.Integer(Convert.ToInt32(wholeandfractional.Item1), false));
                            messageFragments.Add(MessageFragment.Text(folderGallonsRemaining));
                        }
                    }
                    else if (convertLitresToGallons(currentFuel) >= 1)
                    {
                        reportedSomething = true;
                        messageFragments.Add(MessageFragment.Text(folderOneGallonRemaining));
                    }
                    else if (convertLitresToGallons(currentFuel) > 0.5f)
                    {
                        reportedSomething = true;
                        messageFragments.Add(MessageFragment.Text(folderHalfAGallonRemaining));
                    }
                    else if (convertLitresToGallons(currentFuel) > 0)
                    {
                        reportedSomething = true;
                        messageFragments.Add(MessageFragment.Text(folderAboutToRunOut));
                    }
                    if (CrewChief.currentGameState != null && CrewChief.currentGameState.Now != null)
                        lastFuelCall = CrewChief.currentGameState.Now;
                }
                else // !fuelReportsInGallon
                {
                    if (currentFuel >= 2)
                    {
                        reportedSomething = true;
                        messageFragments.Add(MessageFragment.Integer((int)currentFuel, false));
                        messageFragments.Add(MessageFragment.Text(folderLitresRemaining));
                    }
                    else if (currentFuel >= 1)
                    {
                        reportedSomething = true;
                        messageFragments.Add(MessageFragment.Text(folderOneLitreRemaining));
                    }
                    else if (currentFuel > 0)
                    {
                        reportedSomething = true;
                        messageFragments.Add(MessageFragment.Text(folderAboutToRunOut));
                    }
                    if (CrewChief.currentGameState != null && CrewChief.currentGameState.Now != null)
                        lastFuelCall = CrewChief.currentGameState.Now;
                }
            }

            return reportedSomething;
        }

        /// <summary>
        /// "Plenty of fuel" / "Fuel will be tight" / "You need to pit" / etc.
        /// </summary>
        /// <param name="allowNoDataMessage">this is fuel specific command response -
        /// "How's my fuel?" or "Report fuel/battery status"</param>
        /// <param name="isRace"></param>
        public void reportFuelStatus(Boolean allowNoDataMessage, Boolean isRace)
        {
            if (!GlobalBehaviourSettings.enabledMessageTypes.Contains(MessageTypes.FUEL))
            {
                if (allowNoDataMessage)
                {
                    this.audioPlayer.playMessageImmediately(new QueuedMessage(AudioPlayer.folderNoData, 0));
                }
                return;
            }

            var fuelStatusMessageFragments = new List<MessageFragment>();
            Boolean reportedRemaining = reportFuelRemaining(allowNoDataMessage, fuelStatusMessageFragments);
            Boolean reportedConsumption = reportFuelConsumption(false /*individualResponse*/, fuelStatusMessageFragments);
            Boolean reportedFuelNeeded = false;
            Boolean isSufficientTimeToSaveFuel = false;
            Boolean isCloseToRaceEnd = false;
            if (CrewChief.currentGameState != null)
            {
                if (!sessionHasFixedNumberOfLaps)
                {
                    isSufficientTimeToSaveFuel = CrewChief.currentGameState.SessionData.SessionTimeRemaining > 500;
                    isCloseToRaceEnd = (CrewChief.currentGameState.SessionData.SessionTimeRemaining < 120 && CrewChief.currentGameState.SessionData.SessionTimeRemaining > -1)
                        || CrewChief.currentGameState.SessionData.IsLastLap;
                }
                else
                {
                    switch (CrewChief.currentGameState.SessionData.TrackDefinition.trackLengthClass)
                    {
                        case TrackData.TrackLengthClass.VERY_LONG:
                            isSufficientTimeToSaveFuel = lapsRemaining >= 1;
                            isCloseToRaceEnd = lapsRemaining <= 1;
                            break;
                        case TrackData.TrackLengthClass.LONG:
                            isSufficientTimeToSaveFuel = lapsRemaining >= 2;
                            isCloseToRaceEnd = lapsRemaining <= 2;
                            break;
                        case TrackData.TrackLengthClass.MEDIUM:
                            isSufficientTimeToSaveFuel = lapsRemaining >= 4;
                            isCloseToRaceEnd = lapsRemaining <= 2;
                            break;
                        case TrackData.TrackLengthClass.SHORT:
                            isSufficientTimeToSaveFuel = lapsRemaining >= 5;
                            isCloseToRaceEnd = lapsRemaining <= 2;
                            break;
                        case TrackData.TrackLengthClass.VERY_SHORT:
                            isSufficientTimeToSaveFuel = lapsRemaining >= 6;
                            isCloseToRaceEnd = lapsRemaining <= 3;
                            break;
                    }
                }
            }
            if (isRace)
            {
                float extraFuelToEnd = getAdditionalFuelToEndOfRace(false);
                if (extraFuelToEnd != NO_FUEL_DATA)
                {
                    float halfALap = fuelForHalfALap();

                    // extraFuelToEnd to end is a measure of how much fuel we need to add to get to the end. If it's
                    // negative we have fuel to spare
                    if (extraFuelToEnd <= 0 && extraFuelToEnd * -1 < halfALap)
                    {
                        reportedFuelNeeded = true;
                        // we expect to have sufficient fuel, but it'll be tight. LitresToEnd * -1 is how much we expect
                        // to have left over
                        fuelStatusMessageFragments.Add(MessageFragment.Text(folderFuelShouldBeOK));
                    }
                    else if (extraFuelToEnd > 0)
                    {
                        // we need some fuel - see if we might be able stretch it
                        if (extraFuelToEnd < halfALap && isSufficientTimeToSaveFuel)
                        {
                            reportedFuelNeeded = true;
                            // unlikely to make it, we'll have to fuel save
                            fuelStatusMessageFragments.Add(MessageFragment.Text(folderFuelWillBeTight));
                        }
                        else if (!isCloseToRaceEnd)
                        {
                            // we got here by looking at the fuel without any margin, but we actually add margin,
                            // so recalculate so that we report exactly what we would add if the player pitted.
                            extraFuelToEnd = getAdditionalFuelToEndOfRace(true);
                            if (extraFuelToEnd > fuelCapacity)
                            {
                                audioPlayer.playMessage(new QueuedMessage("flags/and", 0, secondsDelay: 4, abstractEvent: this, priority: 10));
                                audioPlayer.playMessage(new QueuedMessage(folderWillNeedToStopAgain, 0, abstractEvent: this, priority: 10));
                                Log.Commentary("And we'll need to stop again (after the next pit stop)");
                            }
                            else if (fuelReportsInGallon)
                            {
                                // for gallons we want both whole and fractional part cause its a stupid unit.
                                float gallonsNeeded = convertLitresToGallons(extraFuelToEnd, true);
                                Tuple<int, int> wholeandfractional = Utilities.WholeAndFractionalPart(gallonsNeeded);
                                if (wholeandfractional.Item2 > 0)
                                {
                                    reportedFuelNeeded = true;
                                    fuelStatusMessageFragments.AddRange(MessageContents(folderWillNeedToAdd, wholeandfractional.Item1, NumberReader.folderPoint, wholeandfractional.Item2, folderGallonsToGetToTheEnd));
                                }
                                else
                                {
                                    reportedFuelNeeded = true;
                                    int wholeGallons = Convert.ToInt32(wholeandfractional.Item1);
                                    fuelStatusMessageFragments.AddRange(MessageContents(wholeGallons, wholeGallons == 1 ? folderWillNeedToAddOneGallonToGetToTheEnd : folderWillNeedToAdd, wholeGallons, folderGallonsToGetToTheEnd));
                                }
                            }
                            else
                            {
                                reportedFuelNeeded = true;
                                fuelStatusMessageFragments.AddRange(MessageContents(folderWillNeedToAdd, (int)Math.Ceiling(extraFuelToEnd), folderLitresToGetToTheEnd));
                            }
                            if (extraFuelToEnd <= fuelCapacity - currentFuel)
                            {
                                fuelStatusMessageFragments.Add(MessageFragment.Text(PitStops.folderMandatoryPitStopsPitWindowOpen));
                            }
                        }
                    }
                    if (reportedFuelNeeded)
                    {
                        if (CrewChief.currentGameState != null && CrewChief.currentGameState.Now != null)
                            lastFuelCall = CrewChief.currentGameState.Now;
                    }
                }
            }

            if (!reportedConsumption && !reportedRemaining && !reportedFuelNeeded && allowNoDataMessage)
            {
                audioPlayer.playMessageImmediately(new QueuedMessage(AudioPlayer.folderNoData, 0));
            }
            else
            {
                if (fuelStatusMessageFragments.Count > 0)
                {
                    if (allowNoDataMessage  // True if this is fuel specific command response.
                        && this.delayResponses && Utilities.random.Next(10) >= 2 && SoundCache.availableSounds.Contains(AudioPlayer.folderStandBy))
                    {
                        this.audioPlayer.pauseQueueAndPlayDelayedImmediateMessage(new QueuedMessage("Fuel/status", 0, messageFragments: fuelStatusMessageFragments), 3 /*lowerDelayBoundInclusive*/, 6 /*upperDelayBound*/);
                    }
                    else
                    {
                        this.audioPlayer.playMessageImmediately(new QueuedMessage("Fuel/status", 0, messageFragments: fuelStatusMessageFragments));
                    }
                }
            }
        }

        /// <summary>
        /// Get a quick n dirty estimate of how many litres for half a lap.
        /// Base this on consumption per lap if we have it, otherwise use track length.
        /// </summary>
        private float fuelForHalfALap()
        {
            float closeFuelAmount = HALF_LAP_RESERVE_DEFAULT;
            float playerBestLapTime = estimateFutureAverageLapTime(CrewChief.currentGameState);
            float averageUsagePerLapToCheck = getConsumptionPerLap();
            if (averageUsagePerLapToCheck > 0)
            {
                closeFuelAmount = averageUsagePerLapToCheck / 2;
            }
            else if (averageUsagePerMinute > 0 && playerBestLapTime > 0)
            {
                closeFuelAmount = (averageUsagePerMinute * playerBestLapTime / 60f) / 2;
            }
            else if (CrewChief.currentGameState != null && CrewChief.currentGameState.SessionData.TrackDefinition != null)
            {
                switch (CrewChief.currentGameState.SessionData.TrackDefinition.trackLengthClass)
                {
                    case TrackData.TrackLengthClass.VERY_SHORT:
                        closeFuelAmount = 1f;
                        break;
                    case TrackData.TrackLengthClass.LONG:
                        closeFuelAmount = 3f;
                        break;
                    case TrackData.TrackLengthClass.VERY_LONG:
                        closeFuelAmount = 4f;
                        break;
                    default:    // SHORT or MEDIUM
                        closeFuelAmount = HALF_LAP_RESERVE_DEFAULT;
                        break;
                }
            }
            return closeFuelAmount;
        }

        /// <summary>
        /// Respond to voice command
        /// </summary>
        /// <param name="voiceMessage">
        /// WHATS_MY_FUEL_USAGE,
        /// WHATS_MY_FUEL_LEVEL,
        /// HOW_MUCH_FUEL_TO_END_OF_RACE,
        /// CALCULATE_FUEL_FOR x LAPS/MINUTES/HOURS,
        /// HOWS_MY_FUEL,
        /// CAR_STATUS/STATUS
        /// </param>
        internal static List<SpeechCommands.ID> Commands = new List<SpeechCommands.ID>
        {
            SpeechCommands.ID.CALCULATE_FUEL_FOR,
            SpeechCommands.ID.CAR_STATUS,
            SpeechCommands.ID.HOWS_MY_FUEL,
            SpeechCommands.ID.HOW_MUCH_FUEL_TO_END_OF_RACE,
            SpeechCommands.ID.SET_FUEL_STRATEGY_CAUTIOUS,
            SpeechCommands.ID.SET_FUEL_STRATEGY_RESET,
            SpeechCommands.ID.SET_FUEL_STRATEGY_RISKY,
            SpeechCommands.ID.STATUS,
            SpeechCommands.ID.WHATS_MY_FUEL_LEVEL,
            SpeechCommands.ID.WHATS_MY_FUEL_USAGE,
        };
        public override SpeechCommands.ID HandlesEvent(String voiceMessage)
        {
            return SpeechCommands.SpeechToCommand(Commands, voiceMessage);
        }

        public override void respond(String voiceMessage)
        {
            respond(voiceMessage, HandlesEvent(voiceMessage));
        }
        public override void respond(String voiceMessage, SpeechCommands.ID cmd)
        {
            if (!Commands.Contains(cmd))
            {
                Log.Error($"{cmd.ToString()} not handled by {this.GetType().Name}");
                return;
            }
            if (!GlobalBehaviourSettings.enabledMessageTypes.Contains(MessageTypes.FUEL))
            {
                this.audioPlayer.playMessageImmediately(new QueuedMessage(AudioPlayer.folderNoData, 0));
                return;
            }

            switch (cmd)
            {
                case SpeechCommands.ID.WHATS_MY_FUEL_USAGE:
                {
                    if (!reportFuelConsumption(true /*individualResponse*/, null /*messageFragments*/))
                    {
                        audioPlayer.playMessageImmediately(new QueuedMessage(AudioPlayer.folderNoData, 0));
                    }

                    break;
                }
                case SpeechCommands.ID.WHATS_MY_FUEL_LEVEL:
                    {
                        if (!fuelUseActive)
                        {
                            audioPlayer.playMessageImmediately(new QueuedMessage(AudioPlayer.folderNoData, 0));
                            return;
                        }

                        var message = new List<MessageFragment>();
                        if (currentFuel < 2)
                        {
                            message.AddRange(MessageContents(folderAboutToRunOut));
                        }
                        else if (fuelReportsInGallon)
                        {
                            message.AddRange(Utilities.WholeAndFractionalMessage(convertLitresToGallons(currentFuel)));
                            message.AddRange(MessageContents(folderGallonsRemaining));
                        }
                        else
                        {
                            message.AddRange(MessageContents((int)currentFuel, folderLitresRemaining));
                        }

                        audioPlayer.playMessageImmediately(new QueuedMessage("fuel_level", 0, message));

                        break;
                    }
                case SpeechCommands.ID.HOW_MUCH_FUEL_TO_END_OF_RACE:
                case SpeechCommands.ID.SET_FUEL_STRATEGY_CAUTIOUS:
                case SpeechCommands.ID.SET_FUEL_STRATEGY_RISKY:
                case SpeechCommands.ID.SET_FUEL_STRATEGY_RESET:
                {
                    switch (cmd)
                    {
                        case SpeechCommands.ID.SET_FUEL_STRATEGY_CAUTIOUS:
                            reserveMultiplier = 2.0f;
                            break;
                        case SpeechCommands.ID.SET_FUEL_STRATEGY_RISKY:
                            reserveMultiplier = 0.5f; // we might even want to go to 0 here
                            break;
                        case SpeechCommands.ID.SET_FUEL_STRATEGY_RESET:
                            reserveMultiplier = 1.0f;
                            break;
                    }

                    float litresNeeded = getAdditionalFuelToEndOfRace(false, verbose: false); // no reserve so we don't suggest a false pit
                    float halfALap = fuelForHalfALap();
                    List<MessageFragment> fragments = new List<MessageFragment>();
                    if (!fuelUseActive || litresNeeded == NO_FUEL_DATA)
                    {
                        audioPlayer.playMessageImmediately(new QueuedMessage(AudioPlayer.folderNoData, 0));
                    }
                    else if (litresNeeded < 0)
                    {
                        if (litresNeeded * -1 > halfALap)
                        {
                            fragments.Add(MessageFragment.Text(folderPlentyOfFuel));
                        }
                        else
                        {
                            fragments.Add(MessageFragment.Text(folderFuelShouldBeOK));
                        }
                    }
                    else
                    {
                        litresNeeded = getAdditionalFuelToEndOfRace(true); // recalculate with reserve for accurate numbers
                        if (litresNeeded > fuelCapacity)
                        {   // Will need to stop at least once
                            fragments.Add(MessageFragment.Text(folderWillNeedToPitForFuelByTimeIntro));
                        }
                        else
                        {
                            fragments.Add(MessageFragment.Text(folderWillNeedToAdd));
                            if (fuelReportsInGallon)
                            {
                                // for gallons we want both whole and fractional part cause its a stupid unit.
                                float gallonsNeeded = convertLitresToGallons(litresNeeded, true);
                                Tuple<int, int> wholeandfractional = Utilities.WholeAndFractionalPart(gallonsNeeded);
                                if (wholeandfractional.Item2 > 0)
                                {
                                    fragments.AddRange(MessageContents(wholeandfractional.Item1, NumberReader.folderPoint, wholeandfractional.Item2, folderGallons));
                                }
                                else
                                {
                                    int wholeGallons = Convert.ToInt32(wholeandfractional.Item1);
                                    fragments.AddRange(MessageContents(wholeGallons, wholeGallons == 1 ? folderGallon : folderGallons));
                                }
                            }
                            else
                            {
                                int roundedLitresNeeded = (int) Math.Ceiling(litresNeeded);
                                fragments.AddRange(MessageContents(roundedLitresNeeded, roundedLitresNeeded == 1 ? folderLitre : folderLitres));
                            }
                        }

                        if (litresNeeded <= fuelCapacity - currentFuel)
                        {
                            fragments.Add(MessageFragment.Text(PitStops.folderMandatoryPitStopsPitWindowOpen));
                        }
                    }

                    if (fragments.Count > 0)
                    {
                        var fuelMessage = new QueuedMessage("fuel_estimate_to_end", 0, messageFragments: fragments);

                        if (delayResponses && Utilities.random.Next(10) >= 2 && SoundCache.availableSounds.Contains(AudioPlayer.folderStandBy))
                        {
                            this.audioPlayer.pauseQueueAndPlayDelayedImmediateMessage(fuelMessage, 5 /*lowerDelayBoundInclusive*/, 8 /*upperDelayBound*/);
                        }
                        else
                        {
                            this.audioPlayer.playMessageImmediately(fuelMessage);
                        }
                    }

                    break;
                }
                case SpeechCommands.ID.CALCULATE_FUEL_FOR:
                { // Laps, minutes or hours
                    int unit = 0;
                    foreach (KeyValuePair<String[], int> entry in SpeechRecogniser.numberToNumber)
                    {
                        foreach (String numberStr in entry.Key)
                        {
                            if (voiceMessage.Contains(" " + numberStr + " "))
                            {
                                unit = entry.Value;
                                break;
                            }
                        }
                    }
                    if (unit == 0)
                    {
                        audioPlayer.playMessageImmediately(new QueuedMessage(AudioPlayer.folderDidntUnderstand, 0));
                        return;
                    }
                    if (SpeechRecogniser.ResultContains(voiceMessage, SpeechRecogniser.LAP) || SpeechRecogniser.ResultContains(voiceMessage, SpeechRecogniser.LAPS))
                    {
                        if (!reportFuelConsumptionForLaps(unit))
                        {
                            audioPlayer.playMessageImmediately(new QueuedMessage(AudioPlayer.folderNoData, 0));
                        }
                    }
                    else if(SpeechRecogniser.ResultContains(voiceMessage, SpeechRecogniser.MINUTE) || SpeechRecogniser.ResultContains(voiceMessage, SpeechRecogniser.MINUTES))
                    {
                        if (!reportFuelConsumptionForTime(0, unit))
                        {
                            audioPlayer.playMessageImmediately(new QueuedMessage(AudioPlayer.folderNoData, 0));
                        }
                    }
                    else if (SpeechRecogniser.ResultContains(voiceMessage, SpeechRecogniser.HOUR) || SpeechRecogniser.ResultContains(voiceMessage, SpeechRecogniser.HOURS))
                    {
                        if (!reportFuelConsumptionForTime(unit, 0))
                        {
                            audioPlayer.playMessageImmediately(new QueuedMessage(AudioPlayer.folderNoData, 0));
                        }
                    }

                    break;
                }
                case SpeechCommands.ID.HOWS_MY_FUEL:
                case SpeechCommands.ID.CAR_STATUS:
                case SpeechCommands.ID.STATUS:
                    reportFuelStatus(cmd == SpeechCommands.ID.HOWS_MY_FUEL,
                        (CrewChief.currentGameState != null && CrewChief.currentGameState.SessionData.SessionType == SessionType.Race));
                    break;
            }
        }

        /// <summary>
        /// Convert litres to a string in selected units and precision
        /// </summary>
        /// <param name="fractions">3 decimal places vs. whole litres/10ths of gals</param>
        /// <returns></returns>
        private string litresToUnits(float litres, bool fractions)
        {
            string fmt = fuelReportsInGallon ?
                (fractions ? "F3" : "F1") :
                (fractions ? "F3" : "F0");
            return fuelReportsInGallon ?
                $"{convertLitresToGallons(litres).ToString(fmt)} gal" :
                $"{litres.ToString(fmt)}L";
        }
        /// <summary>
        /// Get additional litres required to get to the end
        /// </summary>
        /// <param name="addReserve"></param>
        /// <param name="verbose"></param>
        /// <returns>
        /// +ve: Estimated fuel required
        /// -ve: More than enough
        /// NO_FUEL_DATA (int.MaxValue)
        /// </returns>
        public float getAdditionalFuelToEndOfRace(Boolean addReserve, bool verbose = true, int lapAdjustment = 0)
        {
            float additionalLitresNeeded = NO_FUEL_DATA;
            if (fuelUseActive && CrewChief.currentGameState != null)
            {
                // OK, here's where' the fuel calculations are a bit awkward. AverageUsagePerLap and AverageUsagePerMinute
                // are based on your recent consumption. If you're stretching the fuel out towards the pitstop, this is going
                // to skew these quantities and the calculation will assume you'll carry on driving like this, which isn't
                // necessarily the case. So if we're asking for the extraFuelToEnd *with* the reserve, assume we want the overall
                // average consumption, not the recent consumption

                // one additional hack (tweak...) here. The opening laps tend to have lower consumption because we're often checking-up
                // for other cars, drafting, crashing, etc. If we don't have a decent amount of data to offset this skew, then
                // take the max per-lap consumption rather than the average
                float averageUsagePerMinuteForCalculation;
                float averageUsagePerLapForCalculation;
                if (experimentalFuelCalculations)
                {
                    averageUsagePerLapForCalculation = getConsumptionPerLap();
                    averageUsagePerMinuteForCalculation = -1;
                }
                else
                if ((baseCalculationsOnMaxConsumption || GlobalBehaviourSettings.useOvalLogic || sessionHasHadFCY) && maxConsumptionPerLap > 0 && maxConsumptionPerMinute > 0)
                {
                    averageUsagePerLapForCalculation = maxConsumptionPerLap;
                    averageUsagePerMinuteForCalculation = maxConsumptionPerMinute;
                }
                else
                {
                    if (addReserve && historicAverageUsagePerMinute.Count > 0)
                    {
                        // if we're team racing, use the max consumption as we might not get consumption data for laps when we're not driving
                        averageUsagePerMinuteForCalculation = CrewChief.currentGameState.PitData.IsTeamRacing ?
                            maxConsumptionPerMinute : historicAverageUsagePerMinute.Average();
                    }
                    else
                    {
                        averageUsagePerMinuteForCalculation = averageUsagePerMinute;
                    }
                    if (addReserve && historicAverageUsagePerLap.Count > 0)
                    {
                        // for per-lap consumption, get the biggest if we don't have much data or we're team racing
                        averageUsagePerLapForCalculation = CrewChief.currentGameState.PitData.IsTeamRacing || historicAverageUsagePerLap.Count() <= 5 ?
                            maxConsumptionPerLap : historicAverageUsagePerLap.Average();
                    }
                    else
                    {
                        averageUsagePerLapForCalculation = averageUsagePerLap;
                    }
                }

                float reserve = HALF_LAP_RESERVE_DEFAULT;
                if (experimentalFuelCalculations)
                {
                    var trackLengthClass = CrewChief.currentGameState.SessionData.TrackDefinition.trackLengthClass;
                    float extra = experimentalReserveLapsMedium;

                    if (trackLengthClass > TrackData.TrackLengthClass.MEDIUM)
                    {
                        extra = experimentalReserveLapsLong;
                    }
                    else if (trackLengthClass < TrackData.TrackLengthClass.MEDIUM)
                    {
                        extra = experimentalReserveLapsShort;
                    }

                    if (GlobalBehaviourSettings.useOvalLogic)
                    {
                        extra += experimentalExtraLapsOval;
                    }

                    reserve = extra * averageUsagePerLapForCalculation;
                }
                else if (averageUsagePerLapForCalculation > 0 && addAdditionalFuelLaps > 0)
                {
                    reserve = addAdditionalFuelLaps * averageUsagePerLapForCalculation;
                }

                reserve = reserveMultiplier * reserve;

                if (sessionHasFixedNumberOfLaps && averageUsagePerLapForCalculation > 0)
                {
                    float lapsToFill = lapsRemaining;
                    if (experimentalFuelCalculations)
                    {
                        lapsToFill += lapAdjustment;
                    }

                    float totalLitresNeededToEnd = (averageUsagePerLapForCalculation * lapsToFill) + (addReserve ? reserve : 0);

                    float fuelThisLap = 0f;
                    if (experimentalFuelCalculations && !CrewChief.currentGameState.PitData.InPitlane)
                    {
                        // deduct the fuel to get to the pits, which can be very significant at larger tracks.
                        // There's two ways to do this: either we deduct one lap and add the fuel to get
                        // to the end of this lap, or we estimate the full amount and discount the amount
                        // fuel burnt on this lap. We know exactly how much fuel we've burnt this lap
                        // so we prefer to use that approach. In effect, we reset the currentFuel back to what
                        // it was at the start of this lap.
                        if (fuelLevelWindowByLap.Count > 0)
                        {
                            fuelThisLap = Math.Max(0f, fuelLevelWindowByLap.First() - CrewChief.currentGameState.FuelData.FuelLeft);
                        }
                    }

                    additionalLitresNeeded = totalLitresNeededToEnd - (currentFuel + fuelThisLap);
                    if (verbose)
                    {
                        var reserve_part = "";
                        if (addReserve)
                        {
                            reserve_part = $" reserve = {litresToUnits(reserve, true)}";
                        }
                        Log.Fuel(
                            "Use per lap = " + litresToUnits(averageUsagePerLapForCalculation, true) +
                            " laps to fill = " + lapsToFill +
                            " current fuel = " + litresToUnits(currentFuel, true) +
                            " fuel this lap = " + litresToUnits(fuelThisLap, true) +
                            " additional fuel needed = " + litresToUnits(additionalLitresNeeded, true) +
                            reserve_part);
                    }
                }
                else if (experimentalFuelCalculations)
                {
                    // guard
                }
                else if (averageUsagePerMinuteForCalculation > 0)
                {
                    if (CrewChief.currentGameState.SessionData.TrackDefinition != null && addAdditionalFuelLaps <= 0)
                    { // Add a bit even if no additional fuel laps requested
                        TrackData.TrackLengthClass trackLengthClass = CrewChief.currentGameState.SessionData.TrackDefinition.trackLengthClass;
                        if (trackLengthClass < TrackData.TrackLengthClass.MEDIUM)
                        {
                            reserve = 1f;
                        }
                        else switch (trackLengthClass)
                        {
                            case TrackData.TrackLengthClass.LONG:
                                reserve = 3f;
                                break;
                            case TrackData.TrackLengthClass.VERY_LONG:
                                reserve = 4f;
                                break;
                        }
                    }
                    float minutesRemaining = secondsRemaining / 60f;
                    float expectedLapTime = CrewChief.currentGameState.TimingData.getPlayerBestLapTime();
                    if (expectedLapTime <= 0)
                    {
                        expectedLapTime = CrewChief.currentGameState.TimingData.getPlayerClassBestLapTime();
                    }
                    float maxMinutesRemaining = (secondsRemaining + ((extraLapsAfterTimedSessionComplete + 1) *  expectedLapTime)) / 60f;
                    float totalLitresNeededToEnd = 0;
                    if (averageUsagePerLapForCalculation > 0)
                    {
                        totalLitresNeededToEnd = (averageUsagePerMinuteForCalculation * minutesRemaining) +
                            ((extraLapsAfterTimedSessionComplete + 1) * averageUsagePerLapForCalculation) +
                            (addReserve ? reserve : 0);
                    }
                    else
                    {
                        totalLitresNeededToEnd = (averageUsagePerMinuteForCalculation * maxMinutesRemaining) + (addReserve ? reserve : 0);
                    }
                    additionalLitresNeeded = totalLitresNeededToEnd - currentFuel;
                    if (verbose)
                    {
                        var reserve_part = "";
                        if (addReserve && addAdditionalFuelLaps >= 0)
                        {
                            reserve_part = $" including {addAdditionalFuelLaps} laps of additional fuel, {litresToUnits(reserve, false)}";
                        }
                        Log.Fuel("Use per minute = " + litresToUnits(averageUsagePerMinuteForCalculation, true) + " estimated minutes to go (including final lap) = " +
                        maxMinutesRemaining.ToString("F1") + " current fuel = " + litresToUnits(currentFuel, false) + " additional fuel needed = " + litresToUnits(additionalLitresNeeded, false) + 
                        reserve_part);                        
                    }
                }
            }
            return additionalLitresNeeded;
        }

        // We want to estimate the average lap time that we are expected to put in
        // for the rest of the session. Even ignoring changes in weather and track
        // conditions, this is difficult to estimate because the player may be stuck
        // behind traffic, gaining a draft advantage, fuel saving and after pitting
        // they may be on faster / slower tyres and have gained or lost draft.
        //
        // Estimating a lower lap time than what will actually happen is the
        // conservative thing to do and will typically result in sometimes putting
        // one lap extra of fuel in for the refuel. The easiest way to achieve this
        // is to always pick the fastest lap in the session so far, which does attempt
        // to take current track conditions into account. We could go even further
        // and use persisted data including for quali / low fuel runs.
        private float estimateFutureAverageLapTime(GameStateData currentGameState)
        {
            if (currentGameState == null)
            {
                return -1;
            }
            float dirty = currentGameState.SessionData.LapTimePreviousEstimateForInvalidLap;
            float best = currentGameState.TimingData.getPlayerClassBestLapTime();
            float estimated = currentGameState.SessionData.EstimatedLapTime;

            if (estimated > 0 && currentGameState.SessionData.CompletedLaps < 2)
            {
                return estimated;
            }

            if (best > 0)
            {
                return best;
            }
            else if (dirty > 0)
            {
                return dirty;
            }
            else if (estimated > 0)
            {
                return estimated;
            }

            return -1;
        }

        /// <summary>
        /// Try to predict the the earliest possible time/lap and the latest possible time/lap we can come in for our pitstop and still make it to the end.
        /// we need to check if more then one stop is needed to finish the race in this case we dont care about pit window
        /// </summary>
        /// <returns>pit window earliest:latest tuple in laps or minutes</returns>
        private Tuple<int, int> getPredictedPitWindow(GameStateData currentGameState)
        {
            int minLaps;
            switch (currentGameState.SessionData.TrackDefinition.trackLengthClass)
            {
                case TrackData.TrackLengthClass.VERY_LONG:
                    minLaps = sessionHasFixedNumberOfLaps ? 2 : 1;
                    break;
                case TrackData.TrackLengthClass.LONG:
                    minLaps = 2 + Utilities.random.Next(2); // 2 or 3
                    break;
                case TrackData.TrackLengthClass.SHORT:
                    minLaps = 3 + Utilities.random.Next(3); // 3, 4 or 5
                    break;
                case TrackData.TrackLengthClass.VERY_SHORT:
                    minLaps = 4 + Utilities.random.Next(3); // 4, 5 or 6
                    break;
                default:
                    minLaps = 3 + Utilities.random.Next(2); // 3 or 4
                    break;
            }

            Tuple<int, int> pitWindow = new Tuple<int, int>(-1, -1);
            // tweak the fuelCapacity using the requested reserve. That is, the fuel capcity we use for these calcuations is the full
            // tank minus the reserve amount we've set in the Properties
            float averageUsagePerLapToUse = getConsumptionPerLap();
            float fuelCapacityAllowingForReserve;
            if (experimentalFuelCalculations)
            {
                // we do all our reserve calculations in getAdditionalFuelToEndOfRace, no need to duplicate the logic
                fuelCapacityAllowingForReserve = fuelCapacity;
            }
            else if (addAdditionalFuelLaps > 0)
            {
                fuelCapacityAllowingForReserve = fuelCapacity - (averageUsagePerLap * addAdditionalFuelLaps);
            }
            else
            {
                // no reserve set so use the default
                fuelCapacityAllowingForReserve = fuelCapacity - HALF_LAP_RESERVE_DEFAULT;
            }
            if (sessionHasFixedNumberOfLaps)
            {
                if ((experimentalFuelCalculations || lapsCompletedSinceFuelReset >= minLaps) && averageUsagePerLapToUse > 0)
                {
                    float litresNeeded = getAdditionalFuelToEndOfRace(experimentalFuelCalculations);
                    if (litresNeeded > 0)
                    {
                        // more then 1 stop needed
                        if (litresNeeded > fuelCapacityAllowingForReserve)
                        {
                            return pitWindow;
                        }
                        int maximumLapsForFullTankOfFuel = (int)Math.Floor(fuelCapacityAllowingForReserve / averageUsagePerLapToUse);
                        // pit window start is just the total race distance - maximumLapsForFullTankOfFuel. It's the earliest we can stop, fill the tank, and still finish
                        int pitWindowStart = sessionNumberOfLaps - maximumLapsForFullTankOfFuel;
                        // pit window end is just the lap on which we'll run out of fuel if we don't stop
                        int pitWindowEnd = currentGameState.SessionData.CompletedLaps + (int)Math.Floor(currentFuel / averageUsagePerLap);
                        Log.Fuel("calculated fuel window (laps): pitwindowStart = " + pitWindowStart + " pitWindowEnd = " + pitWindowEnd +
                            " maximumLapsForFullTankOfFuel = " + maximumLapsForFullTankOfFuel);

                        // some sanity checks to ensure we're not calling nonsense window data. A negative window start is OK - this just means we're inside the
                        // window at the point where we're doing the calculation
                        if (pitWindowStart <= pitWindowEnd && pitWindowStart < sessionNumberOfLaps && pitWindowEnd > 0)
                        {
                            pitWindow = new Tuple<int, int>(pitWindowStart, pitWindowEnd);
                        }
                    }
                }
            }
            else
            {
                float averageUsagePerMinuteToUse = getConsumptionPerMinute();
                if (lapsCompletedSinceFuelReset >= minLaps && averageUsagePerMinuteToUse > 0)
                {
                    float litresNeeded = getAdditionalFuelToEndOfRace(false);
                    if (litresNeeded > 0)
                    {
                        // more then 1 stop needed
                        if (litresNeeded > fuelCapacityAllowingForReserve)
                        {
                            return pitWindow;
                        }
                        int sessionTotalRunTimeMinutes = (int)Math.Floor(currentGameState.SessionData.SessionTotalRunTime / 60);
                        int sessionRunningTimeMinutes = (int)Math.Floor(currentGameState.SessionData.SessionRunningTime / 60);
                        int maximumMinutesForFullTankOfFuel = (int)Math.Floor(fuelCapacityAllowingForReserve / averageUsagePerMinuteToUse);
                        // pit window start is just the total race time - maximumMinutesForFullTankOfFuel. It's the earliest we can stop, fill the tank, and still finish
                        int pitWindowStart = sessionTotalRunTimeMinutes - maximumMinutesForFullTankOfFuel;
                        // pit window end is just the minute on which we'll run out of fuel if we don't stop
                        int pitWindowEnd = sessionRunningTimeMinutes + (int)Math.Floor(currentFuel / averageUsagePerMinute);
                        Log.Fuel("calculated fuel window (minutes): pitwindowStart = " + pitWindowStart + " pitWindowEnd = " + pitWindowEnd +
                            " maximumMinutesForFullTankOfFuel = " + maximumMinutesForFullTankOfFuel);
                        // some sanity checks to ensure we're not calling nonsense window data. A negative window start is OK - this just means we're inside the
                        // window at the point where we're doing the calculation
                        if (pitWindowStart <= pitWindowEnd && pitWindowStart < sessionTotalRunTimeMinutes && pitWindowEnd > 0)
                        {
                            pitWindow = new Tuple<int, int>(pitWindowStart, pitWindowEnd);
                        }
                    }
                }
            }
            return pitWindow;
        }

        public override int resolveMacroKeyPressCount(String macroName)
        {
            // only used for r3e auto-fuel amount selection at present
            Console.WriteLine("Getting fuel requirement keypress count");
            int litresToEnd = (int) Math.Ceiling(getAdditionalFuelToEndOfRace(true));

            // limit the number of key presses to 200 here, or fuelCapacity
            int fuelCapacityInt = (int)fuelCapacity;
            if (fuelCapacityInt > 0 && fuelCapacityInt - currentFuel < litresToEnd)
            {
                // if we have a known fuel capacity and this is less than the calculated amount of fuel we need, warn about it.
                audioPlayer.playMessage(new QueuedMessage(folderWillNeedToStopAgain, 0, secondsDelay: 4, abstractEvent: this, priority: 10));
            }
            int maxPresses = fuelCapacityInt > 0 ? fuelCapacityInt : 200;
            return litresToEnd < 0 ? 0 : litresToEnd > maxPresses ? maxPresses : litresToEnd;
        }

        private float convertLitresToGallons(float litres, Boolean roundTo1dp = false)
        {
            if (litres <= 0)
            {
                return 0f;
            }
            float gallons = litres / litresPerGallon;
            if(roundTo1dp)
            {
                return ((float)Math.Round(gallons * 10f)) / 10f;
            }
            return gallons;
        }

        private int convertGallonsToLitres(float gallons)
        {
            return (int)Math.Ceiling(gallons * litresPerGallon);
        }

        /// <summary>
        /// must be a valid lap, must not be in pitlane, must have valid laptime data and not be an outlier. Here we assume that an
        /// outlap will be an outlier WRT pace so we don't check that explicitly
        /// </summary>
        private Boolean canUseLastLapForMaxPerLapFuelConsumption(GameStateData currentGameState)
        {
            return currentGameState.SessionData.PreviousLapWasValid && currentGameState.SessionData.LapTimePrevious > 0 &&
                currentGameState.SessionData.PlayerLapTimeSessionBest > 0 && !currentGameState.PitData.InPitlane &&
                currentGameState.SessionData.LapTimePrevious < currentGameState.SessionData.PlayerLapTimeSessionBest + LapTimes.outlierPaceLimits[currentGameState.SessionData.TrackDefinition.trackLengthClass];
        }

        private float getConsumptionPerLap()
        {
            if (experimentalFuelCalculations)
            {
                return averageUsagePerLap;
            }
            return (baseCalculationsOnMaxConsumption || GlobalBehaviourSettings.useOvalLogic || sessionHasHadFCY) && maxConsumptionPerLap > 0 ? maxConsumptionPerLap : averageUsagePerLap;
        }

        // very inefficient implementation of median, if it's a problem we can optimise later
        private static float GetMedian(List<float> input)
        {
            if (input == null || input.Count == 0)
                throw new Exception("Median of empty list not defined.");
            if (input.Count == 1)
            {
                return input[0];
            }

            var sorted = new float[input.Count];
            input.CopyTo(sorted);
            Array.Sort(sorted);

            int size = input.Count;
            int mid = size / 2;
            return (size % 2 != 0) ? sorted[mid] : (sorted[mid] + sorted[mid - 1]) / 2f;
        }

        private float getConsumptionPerMinute()
        {
            return (baseCalculationsOnMaxConsumption || GlobalBehaviourSettings.useOvalLogic || sessionHasHadFCY) && maxConsumptionPerMinute > 0 ? maxConsumptionPerMinute : averageUsagePerMinute;
        }

        /// <summary>
        /// don't allow some automatic fuel messages to play if the last fuel message was less than 30 seconds ago
        /// </summary>
        private static Boolean canPlayFuelMessage()
        {
            return CrewChief.currentGameState != null
                && CrewChief.currentGameState.Now != null
                && CrewChief.currentGameState.Now > lastFuelCall.AddSeconds(30);
        }

        // instead of having two separate algorithms for fuel usage: one based on laps and another based on time,
        // the new algorithm estimates laps remaining in a fixed time race, then has a single codepath for the fuel
        // calculation.
        private void estimateLapsForFixedTimeRace(GameStateData currentGameState)
        {
            // Here we turn the session fixed time into a lap estimate.
            var lapTime = estimateFutureAverageLapTime(currentGameState);
            if (lapTime > 0)
            {
                // One might intuitively think that as soon as the session times out,
                // the next time the leader crosses the line is the end of the race but iRacing requires a white flag lap
                // and that can sometimes result in a lap more than you'd expect. The details are not made public
                // but seem to be based on whether the leader will finish the next lap under the time limit based on
                // their average (or best) lap time.
                //
                // https://www.reddit.com/r/iRacing/comments/pawpiy/how_is_white_flag_calculated_in_timed_events/
                //
                // We add some margin to account for that known case of crossing the line just before the timeout.
                float iRacingMargin = (currentGameState.SessionData.ClassPosition == 1) ? 10f : 0f;

                // This is a very conservative calculation that becomes more conservative further down the field.
                // We should really use the leader's *average* laptime, from when *they* started their lap, and
                // assume that they have the same pit strategy as us.
                float sessionTimeRemaining = currentGameState.SessionData.SessionTimeRemaining + iRacingMargin;

                // but it gets even more complicated for multi-class racing, because the white flag drops for all
                // cars when the fastest car on the field out of all the classes starts their lap. This effectively
                // results in an extension of the session time, but if we can't get a good reading on the fastest
                // time in the session, and we're not in the fastest class, then we have to play it safe and assume
                // there is one more lap to go for us.
                int multiclass_extra_laps = 0;
                if (GameStateData.Multiclass && currentGameState.getOpponentAtOverallPosition(1) != null &&
                    !CarData.IsCarClassEqual(currentGameState.carClass, currentGameState.getOpponentAtOverallPosition(1).CarClass))
                {
                    // this might not be set due to lack of telemetry data or nobody putting in a clean lap
                    // (likely on long tracks). Generally this is very conservative, we could consider optimising
                    // the estimate by looking at when the leader last crossed the start/finish line and deducting
                    // pitstop benchmarks for that class. But we don't want to take any risks.
                    float overall_best = currentGameState.SessionData.OverallSessionBestLapTime;
                    if (overall_best <= 0)
                    {
                        overall_best = currentGameState.getOpponentAtOverallPosition(1).EstimatedLapTime;
                    }
                    if (overall_best > 0 && overall_best < lapTime)
                    {
                        float correction = overall_best / 2;
                        Log.Fuel("EXPERIMENTAL correcting session time for faster class, adding " + correction + " for lap time of " + overall_best);
                        sessionTimeRemaining += correction;
                    }
                    else
                    {
                        Log.Fuel("EXPERIMENTAL couldn't estimate the fastest laps in the session, assuming we need one more lap");
                        multiclass_extra_laps = 1;
                    }
                }

                if (Strategy.persistBenchmarks && !currentGameState.PitData.InPitlane)
                {
                    // If the player only saved a full service (or that's what all the other cars have done) we may underestimate
                    float extra = getAdditionalFuelToEndOfRace(false, verbose: false);
                    var benchmark = BenchmarkHelper.findPersistedBenchmarkForCombo(extra);
                    if (extra >= 5 && extra != NO_FUEL_DATA && benchmark != null && benchmark.timeLoss > 0)
                    {
                        int pitstops = (int)Math.Ceiling(extra / fuelCapacity);
                        float loss = benchmark.timeLoss * pitstops;
                        Log.Fuel("EXPERIMENTAL deducting pitstop time loss of " + loss + " for " + pitstops + " stop(s)");
                        sessionTimeRemaining -= loss;
                    }
                }

                // there's another optimisation we could make here, but it's a bit risky. If our delta to the leader is quite significant,
                // we could end up doing one lap less than we are predicting. To account for this, we could deduct the delta from the race
                // time. However, the risk is that the leader crashes out and then we lead the race but need to do one more lap and run
                // out of fuel. We play it safe and sometimes over-estimate by a lap, but perhaps the "risky" fuel strategy could alter
                // the behaviour here.

                sessionTimeRemaining = Math.Max(0, sessionTimeRemaining);

                // incase we were one off
                if (currentGameState.SessionData.IsLastLap)
                {
                    lapsRemaining = 1;
                }
                else
                {
                    lapsRemaining = multiclass_extra_laps + extraLapsAfterTimedSessionComplete + (int)Math.Ceiling(sessionTimeRemaining / lapTime);
                }
                sessionNumberOfLaps = currentGameState.SessionData.CompletedLaps + lapsRemaining;
                sessionHasFixedNumberOfLaps = true;

                Log.Fuel("EXPERIMENTAL estimating " + lapsRemaining + " laps remaining (total " + sessionNumberOfLaps + ") from a lap time of " + lapTime + " for " + sessionTimeRemaining + " remaining");

                float perLap = getConsumptionPerLap();
                LapsRemaining = perLap == 0 || lapsRemaining * perLap > currentFuel ? 0 : lapsRemaining;
                LapsUntilRefuel = LapsRemaining > 0 ? 0 : (int)(currentFuel / perLap);
            }
        }

        /// <summary>
        /// Return the probable number of remaining laps in a timed race
        /// 0 => more than we can manage without pitting
        /// </summary>
        public static int LapsRemaining { get ; private set; }
        /// <summary>
        /// Return the probable number of remaining laps until we have to refuel
        /// 0 => We don't have to refuel
        /// </summary>
        public static int LapsUntilRefuel { get; private set; }

        // this is where we apply the experimental fuel percentile values, saving it into the legacy "average" field
        private void updateAverageFuelUsage(GameStateData currentGameState)
        {
            if (historicAverageUsagePerLap.Count + persistedAverageUsagePerLap.Count > 0)
            {
                var all_data = new List<float>();
                all_data.AddRange(historicAverageUsagePerLap);
                all_data.AddRange(persistedAverageUsagePerLap);
                float max_usage = all_data.Max();
                if (GlobalBehaviourSettings.useOvalLogic || experimentalFuelPercentile >= 100)
                {
                    averageUsagePerLap = max_usage;
                }
                else
                {
                    // rescale to 0.0 = median, 1.0 = max
                    float experimentalFuelScale = (Math.Max(50f, Math.Min(100f, experimentalFuelPercentile)) - 50f) / 50f;
                    float median = GetMedian(all_data);
                    // "average" is a lie, but we're hijacking an existing field to retrofit the new algorithm. This
                    // is more like a value somewhere between the median (P50) and max lap time (P100). We could
                    // calculate more accurate percentiles if we detect that we have a lot of data.
                    averageUsagePerLap = median + experimentalFuelScale * (max_usage - median);
                }
                // Log.Fuel("EXPERIMENTAL use per lap=" + averageUsagePerLap + " left=" + litresToUnits(currentGameState.FuelData.FuelLeft, false));
            }
        }
    }

    class PersistedFuel
    {
        public String game { get; set; }
        public String carName { get; set; }
        public String trackName { get; set; }
        public List<float> fuel { get; set; }

        public PersistedFuel()
        {
            fuel = new List<float>();
        }

        public Boolean isForThisCombo(String game, String carName, String trackName)
        {
            return this.game != null && this.game.Equals(game) &&
                this.carName != null && this.carName.Equals(carName) &&
                this.trackName != null && this.trackName.Equals(trackName);
        }
    }
}
